package com.alevel.java.ubike;

import static org.junit.jupiter.api.Assertions.*;

import com.alevel.java.ubike.config.ConnectionProvider;
import com.alevel.java.ubike.config.PropertiesConfig;
import com.alevel.java.ubike.exception.UbikeReportException;
import com.alevel.java.ubike.report.ReportFactory;
import java.sql.SQLException;
import java.util.Properties;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReportAppTest {

    private ReportFactory reportFactory;
    private PropertiesConfig propertiesConfig;
    private Properties properties;

    @BeforeEach
    void setUp() {
        propertiesConfig = new PropertiesConfig();
        properties = propertiesConfig.jdbcProperties();
    }

    @AfterEach
    void tearDown() {
        reportFactory = null;
        propertiesConfig = null;
        properties = null;
    }

    @Test
    public void ForPilotCountOfUsage_duringLastMonth_is_2() throws SQLException, UbikeReportException {
        String user = "pilot";
        int result;
        try (ConnectionProvider connectionProvider = new ConnectionProvider(properties)) {
            reportFactory = new ReportFactory(connectionProvider);
            result = reportFactory.countOfDistinctVehiclesByNick(user).load();
        }
        assertEquals(2, result);
    }
    @Test
    public void ForPilot_IO_CountOfUsage_duringLastMonth_is_1() throws SQLException, UbikeReportException {
        String user = "pilot_IO";
        int result;
        try (ConnectionProvider connectionProvider = new ConnectionProvider(properties)) {
            reportFactory = new ReportFactory(connectionProvider);
            result = reportFactory.countOfDistinctVehiclesByNick(user).load();
        }
        assertEquals(1, result);
    }
    @Test
    public void ForPilot_totalRides_is3() throws SQLException, UbikeReportException {
        String user = "pilot";
        int result;
        try (ConnectionProvider connectionProvider = new ConnectionProvider(properties)) {
            reportFactory = new ReportFactory(connectionProvider);
            result = reportFactory.countOfRidesByRiderNick(user).load();
        }
        assertEquals(3, result);
    }
    @Test
    public void ForPilot_IO_totalRides_is1() throws SQLException, UbikeReportException {
        String user = "pilot_IO";
        int result;
        try (ConnectionProvider connectionProvider = new ConnectionProvider(properties)) {
            reportFactory = new ReportFactory(connectionProvider);
            result = reportFactory.countOfRidesByRiderNick(user).load();
        }
        assertEquals(1, result);
    }
}