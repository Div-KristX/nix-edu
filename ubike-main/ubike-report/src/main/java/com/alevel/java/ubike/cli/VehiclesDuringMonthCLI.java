package com.alevel.java.ubike.cli;

import com.alevel.java.ubike.exception.UbikeReportException;
import com.alevel.java.ubike.report.ReportFactory;
import java.util.Scanner;

public class VehiclesDuringMonthCLI implements InteractiveCLI {

    private final ReportFactory reportFactory;

    public VehiclesDuringMonthCLI(ReportFactory reportFactory) {
        this.reportFactory = reportFactory;
    }

    @Override
    public void run() throws UbikeReportException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a nickname ");
        String nickname = scanner.nextLine();
        int result = reportFactory.countOfDistinctVehiclesByNick(nickname).load();
        System.out.printf("Count of the vehicles for user: %s - %d", nickname, result);
    }
}
