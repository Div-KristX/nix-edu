package com.alevel.java.ubike.cli;

import com.alevel.java.ubike.exception.UbikeReportException;
import com.alevel.java.ubike.report.ReportFactory;
import java.util.Scanner;

public class CountOfRidesCLI implements InteractiveCLI {

    private final ReportFactory reportFactory;

    public CountOfRidesCLI(ReportFactory reportFactory) {
        this.reportFactory = reportFactory;
    }

    @Override
    public void run() throws UbikeReportException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a nickname ");
        String nickname = scanner.nextLine();
        int result = reportFactory.countOfRidesByRiderNick(nickname).load();
        System.out.printf("Count of the rides by user: %s - %d", nickname, result);
    }
}
