package com.alevel.java.ubike.report;

import com.alevel.java.ubike.exception.UbikeReportException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.function.Supplier;

public class VehiclesDuringMonth implements Report<Integer> {

    private final Supplier<Connection> connectionSupplier;
    private final String riderNickName;

    public VehiclesDuringMonth(Supplier<Connection> connectionSupplier, String riderNickName) {
        this.connectionSupplier = connectionSupplier;
        this.riderNickName = riderNickName;
    }

    @Override
    public Integer load() throws UbikeReportException {
        LocalDate date = LocalDate.now();
        date = date.minusMonths(1);
        Timestamp lastMonth = Timestamp.from(
            LocalDate
                .of(date.getYear(), date.getMonth(), 1)
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC));
        Timestamp endOfTheMonth = Timestamp.from(
            LocalDate
                .of(date.getYear(), date.getMonth(), date.getMonth().maxLength())
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC));
        String sql = """
            select count(vehicle_id) as result
            from (select distinct vehicle_id from rides as rd
            join riders r on rd.rider_id = r.id
            where r.nickname = ? and rd.started_at between ? and ?) as rrvi;
            """;
        try (PreparedStatement query = connectionSupplier.get().prepareStatement(sql)) {
            query.setString(1, riderNickName);
            query.setTimestamp(2, lastMonth);
            query.setTimestamp(3, endOfTheMonth);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new UbikeReportException("No result found");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
