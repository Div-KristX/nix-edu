package com.alevel.java.ubike.report;

import com.alevel.java.ubike.exception.UbikeReportException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Supplier;

public class CountOfRides implements Report<Integer> {

    private final Supplier<Connection> connectionSupplier;
    private final String riderNickName;

    public CountOfRides(Supplier<Connection> connectionSupplier, String riderNickName) {
        this.connectionSupplier = connectionSupplier;
        this.riderNickName = riderNickName;
    }

    @Override
    public Integer load() throws UbikeReportException {
        String sql = """
            select count(rider_id) as result from rides as rd
            join riders r on r.id = rd.rider_id
            where r.nickname = ?;
            """;
        try (PreparedStatement query = connectionSupplier.get().prepareStatement(sql)) {
            query.setString(1, riderNickName);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new UbikeReportException("No result found");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
