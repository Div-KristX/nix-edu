package com.alevel.java.ubike.cli;

import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.SpecificFactories.VehicleFactory;
import com.alevel.java.ubike.command.data.CreateVehicleRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.dto.VehicleDTO;
import java.util.Scanner;

public class IngestVehicleInteractiveCLI {

    private final VehicleFactory vehicleFactory;

    public IngestVehicleInteractiveCLI(VehicleFactory vehicleFactory) {
        this.vehicleFactory = vehicleFactory;
    }

    public void run() throws UbikeIngestException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter location Id:");
        long locationId = scanner.nextLong();
        Command<VehicleDTO> command = vehicleFactory.ingestVehicle(
            new CreateVehicleRequest(locationId));
        VehicleDTO vehicleDTO = command.execute();
        System.out.printf(
            "Created Vehicle id - %d, location altitude %f, longitude %f",
            vehicleDTO.id(),
            vehicleDTO.location().altitude(),
            vehicleDTO.location().longitude()
        );
    }
}
