package com.alevel.java.ubike.cli;

import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.SpecificFactories.RiderFactory;
import com.alevel.java.ubike.command.data.CreateRiderRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.dto.RiderDTO;
import java.util.Scanner;

public class IngestRiderInteractiveCLI {

    private final RiderFactory commandFactory;

    public IngestRiderInteractiveCLI(RiderFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void run() throws UbikeIngestException {
        var scanner = new Scanner(System.in);
        System.out.println("Enter rider nickname:");
        String riderNickname = scanner.nextLine();
        Command<RiderDTO> command = commandFactory.ingestRider(
            new CreateRiderRequest(riderNickname));

        RiderDTO rider = command.execute();

        System.out.printf(
            "Created Rider with id - %d and nickname - %s",
            rider.id(),
            rider.nickName()
        );
    }
}
