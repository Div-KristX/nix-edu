package com.alevel.java.ubike.model.dto;


public record WaypointDTO(
    Long id,
    Coordinates coordinates
) {}
