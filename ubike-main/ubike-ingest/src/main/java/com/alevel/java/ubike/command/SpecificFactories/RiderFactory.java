package com.alevel.java.ubike.command.SpecificFactories;

import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.IngestRiderCommand;
import com.alevel.java.ubike.command.data.CreateRiderRequest;
import com.alevel.java.ubike.model.dto.RiderDTO;
import org.hibernate.SessionFactory;

public class RiderFactory {

    private final SessionFactory sessionFactory;

    public RiderFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Command<RiderDTO> ingestRider(CreateRiderRequest context) {
        return new IngestRiderCommand(sessionFactory, context);
    }
}
