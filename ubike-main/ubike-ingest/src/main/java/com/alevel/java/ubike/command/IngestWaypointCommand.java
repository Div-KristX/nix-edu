package com.alevel.java.ubike.command;

import com.alevel.java.ubike.command.data.CreateWaypointRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.Waypoint;
import com.alevel.java.ubike.model.dto.Coordinates;
import com.alevel.java.ubike.model.dto.WaypointDTO;
import jakarta.persistence.EntityTransaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class IngestWaypointCommand implements Command<WaypointDTO> {

    private final SessionFactory sessionFactory;
    private final CreateWaypointRequest context;

    public IngestWaypointCommand(SessionFactory sessionFactory, CreateWaypointRequest context) {
        this.sessionFactory = sessionFactory;
        this.context = context;
    }


    @Override
    public WaypointDTO execute() throws UbikeIngestException {
        EntityTransaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Waypoint waypoint = new Waypoint();
            waypoint.setAltitude(context.altitude());
            waypoint.setLongitude(context.longitude());
            session.persist(waypoint);
            WaypointDTO result = new WaypointDTO(
                waypoint.getId(),
                new Coordinates(waypoint.getAltitude(), waypoint.getLongitude()));
            tx.commit();
            return result;
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw new UbikeIngestException(e);
        }
    }
}
