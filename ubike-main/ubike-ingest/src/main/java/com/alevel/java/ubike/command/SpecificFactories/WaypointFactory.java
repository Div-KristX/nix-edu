package com.alevel.java.ubike.command.SpecificFactories;

import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.IngestWaypointCommand;
import com.alevel.java.ubike.command.data.CreateWaypointRequest;
import com.alevel.java.ubike.model.dto.WaypointDTO;
import org.hibernate.SessionFactory;

public class WaypointFactory {

    private final SessionFactory sessionFactory;

    public WaypointFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Command<WaypointDTO> ingestWaypoint(CreateWaypointRequest context) {
        return new IngestWaypointCommand(sessionFactory, context);
    }
}
