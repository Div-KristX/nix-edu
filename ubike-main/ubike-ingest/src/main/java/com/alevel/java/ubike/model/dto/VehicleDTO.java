package com.alevel.java.ubike.model.dto;

import com.alevel.java.ubike.model.Ride;
import java.util.ArrayList;
import java.util.List;

public record VehicleDTO(
    Long id,
    Coordinates location,
    List<Ride> rides
) {
    public VehicleDTO(Long id, Coordinates location) {
        this(id, location, new ArrayList<>());
    }
}
