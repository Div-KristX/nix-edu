package com.alevel.java.ubike.command.SpecificFactories;

import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.IngestVehicleCommand;
import com.alevel.java.ubike.command.data.CreateVehicleRequest;
import com.alevel.java.ubike.model.dto.VehicleDTO;
import org.hibernate.SessionFactory;

public class VehicleFactory {

    private final SessionFactory sessionFactory;

    public VehicleFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public Command<VehicleDTO> ingestVehicle(CreateVehicleRequest context) {
        return new IngestVehicleCommand(sessionFactory, context);
    }
}
