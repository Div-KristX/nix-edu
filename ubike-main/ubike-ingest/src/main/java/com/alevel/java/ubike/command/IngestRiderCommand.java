package com.alevel.java.ubike.command;

import com.alevel.java.ubike.command.data.CreateRiderRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.Rider;
import com.alevel.java.ubike.model.dto.RiderDTO;
import jakarta.persistence.EntityTransaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class IngestRiderCommand implements Command<RiderDTO> {

    private final SessionFactory sessionFactory;
    private final CreateRiderRequest context;

    public IngestRiderCommand(SessionFactory sessionFactory, CreateRiderRequest context) {
        this.sessionFactory = sessionFactory;
        this.context = context;
    }

    @Override
    public RiderDTO execute() throws UbikeIngestException {
        EntityTransaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Rider rider = new Rider();
            rider.setNickname(context.nickName());
            session.persist(rider);
            RiderDTO result = new RiderDTO(rider.getId(), rider.getNickname());
            tx.commit();
            return result;
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw new UbikeIngestException(e);
        }
    }
}
