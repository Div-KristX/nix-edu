package com.alevel.java.ubike;

import com.alevel.java.ubike.cli.userInterface.UserInterface;
import java.util.Scanner;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IngestApp {

    private static final Logger log = LoggerFactory.getLogger(IngestApp.class);

    public static void main(String[] args) {
        new IngestApp().run();
    }

    public void run() {
        try (var serviceRegistry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
             var sessionFactory = new Configuration().buildSessionFactory(serviceRegistry)) {
            Scanner scanner = new Scanner(System.in);
            UserInterface userInterface = new UserInterface(sessionFactory, scanner);
            userInterface.initCreateCommand();
        } catch (Exception e) {
            log.error("Error during user interaction", e);
        }
    }

}
