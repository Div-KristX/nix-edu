package com.alevel.java.ubike.cli;


import com.alevel.java.ubike.command.Command;
import com.alevel.java.ubike.command.SpecificFactories.WaypointFactory;
import com.alevel.java.ubike.command.data.CreateWaypointRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.dto.WaypointDTO;
import java.util.Scanner;

public class IngestWaypointInteractiveCLI {

    private final WaypointFactory waypointFactory;

    public IngestWaypointInteractiveCLI(WaypointFactory waypointFactory) {
        this.waypointFactory = waypointFactory;
    }

    public void run() throws UbikeIngestException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter altitude:");
        double altitude = scanner.nextDouble();
        checkCoordinates(altitude);
        scanner.nextLine();
        System.out.print("Enter longitude:");
        double longitude = scanner.nextDouble();
        checkCoordinates(longitude);
        scanner.nextLine();

        Command<WaypointDTO> command = waypointFactory.ingestWaypoint(
            new CreateWaypointRequest(altitude, longitude));
        WaypointDTO waypointDTO = command.execute();

        System.out.printf(
            "Created Waypoint id - %d altitude - %f, longitude - %f",
            waypointDTO.id(),
            waypointDTO.coordinates().altitude(),
            waypointDTO.coordinates().longitude()
        );
    }

    private void checkCoordinates(double coordinates) {
        if (coordinates > 180 || coordinates < -180) {
            throw new IllegalArgumentException("Coordinate is not valid - " + coordinates);
        }
    }
}
