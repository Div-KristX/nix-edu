package com.alevel.java.ubike.command;

import com.alevel.java.ubike.command.data.CreateVehicleRequest;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import com.alevel.java.ubike.model.Vehicle;
import com.alevel.java.ubike.model.Waypoint;
import com.alevel.java.ubike.model.dto.Coordinates;
import com.alevel.java.ubike.model.dto.VehicleDTO;
import jakarta.persistence.EntityTransaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class IngestVehicleCommand implements Command<VehicleDTO> {

    private final SessionFactory sessionFactory;
    private final CreateVehicleRequest context;

    public IngestVehicleCommand(SessionFactory sessionFactory, CreateVehicleRequest context) {
        this.sessionFactory = sessionFactory;
        this.context = context;
    }


    @Override
    public VehicleDTO execute() throws UbikeIngestException {
        EntityTransaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Waypoint waypoint = session.find(Waypoint.class, context.locationId());
            if (waypoint == null) {
                throw new UbikeIngestException("No waypoint found " + context.locationId());
            }
            Vehicle vehicle = new Vehicle();
            vehicle.setLocation(waypoint);
            session.persist(vehicle);
            VehicleDTO result = new VehicleDTO(
                vehicle.getId(),
                new Coordinates(vehicle.getLocation().getAltitude(),
                    vehicle.getLocation().getLongitude()));
            tx.commit();
            return result;
        } catch (UbikeIngestException e) {
            throw e;
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw new UbikeIngestException(e);
        }
    }
}
