package com.alevel.java.ubike.cli.userInterface;

import com.alevel.java.ubike.cli.IngestRideInteractiveCLI;
import com.alevel.java.ubike.cli.IngestRiderInteractiveCLI;
import com.alevel.java.ubike.cli.IngestVehicleInteractiveCLI;
import com.alevel.java.ubike.cli.IngestWaypointInteractiveCLI;
import com.alevel.java.ubike.command.CommandFactory;
import com.alevel.java.ubike.command.SpecificFactories.RiderFactory;
import com.alevel.java.ubike.command.SpecificFactories.VehicleFactory;
import com.alevel.java.ubike.command.SpecificFactories.WaypointFactory;
import com.alevel.java.ubike.exceptions.UbikeIngestException;
import java.util.Scanner;
import org.hibernate.SessionFactory;

public class UserInterface {

    private final SessionFactory sessionFactory;
    private final Scanner scanner;

    public UserInterface(SessionFactory sessionFactory, Scanner scanner) {
        this.sessionFactory = sessionFactory;
        this.scanner = scanner;
    }

    public void initCreateCommand() throws UbikeIngestException {
        System.out.print("""
            Choose command:
            0 -> Create rider
            1 -> Create vehicle
            2 -> Create waypoint
            3 -> Create ride
            Command:""");
        int commandId = scanner.nextInt();
        switch (commandId) {
            case 0 -> new IngestRiderInteractiveCLI(new RiderFactory(sessionFactory)).run();
            case 1 -> new IngestVehicleInteractiveCLI(new VehicleFactory(sessionFactory)).run();
            case 2 -> new IngestWaypointInteractiveCLI(new WaypointFactory(sessionFactory)).run();
            case 3 -> new IngestRideInteractiveCLI(new CommandFactory(sessionFactory)).run();
            default -> {
                System.out.println("Command id is not correct " + commandId);
                initCreateCommand();
            }
        }
        scanner.close();
    }
}

