/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

import java.util.Objects;
import org.junit.*;

/**
 * The {@code AggregatorTest} class
 *
 * @author Krasnov Vladyslav
 */
public class AggregatorTest {

    private Aggregator<Double, Integer> avgAggregator;
    private Aggregator<Integer, Integer> maxAggregator;
    private Aggregator<Integer, testInnerClass> distinctAggregator;
    private Aggregator<String, Object> csvAggregator;
    private Integer[] arrayOfInteger;
    private testInnerClass[] arrayOfObjects = new testInnerClass[3];


    @Before
    public void setUp() {
        arrayOfInteger = new Integer[]{1, 5, 7, 8, 9, 0, 12};
        arrayOfObjects[0] = new testInnerClass(1, "str1");
        arrayOfObjects[1] = new testInnerClass(2, "str2");
        arrayOfObjects[2] = new testInnerClass(2, "str2");
    }

    @After
    public void tearDown() {
        avgAggregator = null;
        maxAggregator = null;
        csvAggregator = null;
        arrayOfInteger = null;
        arrayOfObjects = null;
        distinctAggregator = null;
    }

    @Test
    public void aggregateAVG() {
        avgAggregator = new AvgAggregator<>();
        System.out.println(avgAggregator.aggregate(arrayOfInteger));
    }

    @Test
    public void aggregateMax() {
        maxAggregator = new MaxAggregator<>();
        System.out.println(maxAggregator.aggregate(arrayOfInteger));
    }

    @Test
    public void aggregateCSV() {
        csvAggregator = new CSVAggregator<>();
        System.out.println(csvAggregator.aggregate(arrayOfObjects));
    }

    @Test
    public void aggregateDistinct() {
        distinctAggregator = new DistinctAggregator<>();
        System.out.println(distinctAggregator.aggregate(arrayOfObjects));
    }

    private record testInnerClass(int ID, String name) {

        @Override
        public String toString() {
            return "testInnerClass{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            testInnerClass that = (testInnerClass) o;
            return ID == that.ID && Objects.equals(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(ID, name);
        }
    }
}