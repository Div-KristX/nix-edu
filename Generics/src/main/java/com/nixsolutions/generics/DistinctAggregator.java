/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The {@code DistinctAggregator} class
 *
 * @author Krasnov Vladyslav
 */
public class DistinctAggregator<T> implements Aggregator<Integer, T> {

    /**
     * The method finds a unique value in the array.
     *
     * @param items input array
     * @return The unique value.
     */
    @Override
    public Integer aggregate(T[] items) {
        Set<T> unique = new HashSet<>(List.of(items));
        return unique.size();
    }
}
