/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

/**
 * The {@code AvgAggregator} class
 *
 * @author Krasnov Vladyslav
 */
public class AvgAggregator<T extends Number> implements Aggregator<Double, T> {

    /**
     * The method calculates the average value in the array of numbers.
     *
     * @param items input array
     * @return The average value.
     */
    @Override
    public Double aggregate(T[] items) {
        double avg = 0;
        for (T single : items) {
            avg += single.doubleValue();
        }
        return avg / items.length;
    }
}
