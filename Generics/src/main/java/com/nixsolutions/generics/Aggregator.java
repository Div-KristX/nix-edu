/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

/**
 * The {@code  Aggregator} interface
 *
 * @author Krasnov Vladyslav
 */
public interface Aggregator<A, T> {

    /**
     * The method to implement.
     *
     * @param items input array
     * @return required type
     */
    A aggregate(T[] items);
}
