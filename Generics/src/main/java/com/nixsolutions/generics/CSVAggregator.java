/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

import java.util.StringJoiner;

/**
 * The {@code CSVAggregator} class
 *
 * @author Krasnov Vladyslav
 */
public class CSVAggregator<T> implements Aggregator<String, T> {

    /**
     * The method concat classes toString return, separated by comma.
     *
     * @param items input array
     * @return concat toString methods, separated by comma.
     */

    @Override
    public String aggregate(T[] items) {
        StringJoiner concatObjectsToSting = new StringJoiner(", ");
        for (T single : items) {
            concatObjectsToSting.add(single.toString());
        }
        return concatObjectsToSting.toString();
    }
}
