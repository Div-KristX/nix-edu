/*
 * Copyright (c) 2022,
 * For NIX Solution
 */

package com.nixsolutions.generics;

/**
 * The {@code MaxAggregator} class
 *
 * @author Krasnov Vladyslav
 */
public class MaxAggregator<T extends Comparable<T>> implements Aggregator<T, T> {


    /**
     * The method finds the maximum value in the array.
     *
     * @param items the input array
     * @return max value.
     */
    @Override
    public T aggregate(T[] items) {
        T exampleToCompare = items[0];
        for (T single : items) {
            if (single.compareTo(exampleToCompare) > 0) {
                exampleToCompare = single;
            }
        }
        return exampleToCompare;
    }
}
