-- CREATE TABLES --

create table colors
(
    id   bigserial primary key,
    name text unique not null
);

create table lights
(
    id       bigserial primary key,
    label    text unique not null,
    color_id bigint references colors (id),
    enabled  bool not null default false
);

create table color_history
(
    id           bigserial primary key,
    light_id     bigint references lights (id),
    old_color_id bigint references colors (id),
    new_color_id bigint references colors (id),
    changed_at   timestamp
);

