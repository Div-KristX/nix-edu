-- FILL TABLES WITH DEV DATA --

insert into colors (name)
values ('red');

insert into colors (name)
values ('green');

insert into colors (name)
values ('blue');

insert into colors (name)
values ('pink');

insert into lights (label, color_id)
values ('Sun', 1);

insert into lights (label, color_id)
values ('Lamp', 3);

insert into lights (label, color_id)
values ('IO', 4);


