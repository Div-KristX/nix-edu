package com.nixsolutions.lightshow.simulator;

import com.nixsolutions.lightshow.exceptions.LightshowRecordException;
import com.nixsolutions.lightshow.model.Color;
import com.nixsolutions.lightshow.model.Light;
import com.nixsolutions.lightshow.recorder.LightUpdateRecorder;
import com.nixsolutions.lightshow.recorder.LightsHistoryRecorder;
import com.nixsolutions.lightshow.recorder.Recorder;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightshowSimulator implements Simulator {

    private static final Logger log = LoggerFactory.getLogger(LightshowSimulator.class);
    private final List<Color> colors;

    private final long interval;

    private final int switchCount;

    private final Light light;

    private final SessionFactory sessionFactory;


    public LightshowSimulator(SessionFactory sessionFactory, List<Color> colors, Light light,
        long interval, int switchCount) {
        this.colors = colors;
        this.interval = interval;
        this.switchCount = switchCount;
        this.light = light;
        this.sessionFactory = sessionFactory;

        if (light.getEnabled() == null) {
            Recorder lightsRecorder;
            light.setEnabled(true);
            light.setCurrentColor(colors.get(new Random().nextInt((colors.size() - 1))));
            lightsRecorder = new LightUpdateRecorder(light, sessionFactory, true);
            try {
                lightsRecorder.record();
            } catch (LightshowRecordException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void simulate() {
        int maxRandomNumber = colors.size() - 1;

        StringBuilder allHistory = new StringBuilder();
        allHistory.append("Light '")
            .append(light.getLabel())
            .append("' changed color '")
            .append(light.getCurrentColor().getName())
            .append("'");
        Random colorRandomizer = new Random();

        light.setEnabled(true);

        for (int i = 0; i < switchCount; i++) {
            Color color = light.getCurrentColor();

            Color nextColor = colors.get(colorRandomizer.nextInt(maxRandomNumber));

            while (nextColor.equals(color)) {
                nextColor = colors.get(colorRandomizer.nextInt(maxRandomNumber));
            }
            light.setCurrentColor(nextColor);
            try {
                Timestamp timestamp = Timestamp.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
                new LightUpdateRecorder(light, sessionFactory, false).record();

                log.info("Light '{}' changed color from '{}' to '{}' at {{{}}}",
                    light.getLabel(), color.getName(), nextColor.getName(), timestamp);

                new LightsHistoryRecorder(light, color, nextColor, timestamp, sessionFactory).record();

                allHistory.append(" => '").append(nextColor.getName()).append("'");
            } catch (LightshowRecordException e) {
                throw new RuntimeException(e);
            }
            try {
                Thread.sleep(interval * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println(allHistory);
        log.info("Full history {}", allHistory);
        light.setEnabled(false);
        try {
            new LightUpdateRecorder(light, sessionFactory, false).record();
        } catch (LightshowRecordException e) {
            throw new RuntimeException(e);
        }
    }
}
