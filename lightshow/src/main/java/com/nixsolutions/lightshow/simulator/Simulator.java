package com.nixsolutions.lightshow.simulator;

public interface Simulator {

    void simulate();
}
