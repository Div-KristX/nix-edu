package com.nixsolutions.lightshow.controller;

public interface DBController<T> {

    T getData();
}
