package com.nixsolutions.lightshow.controller;

import com.nixsolutions.lightshow.model.Color;
import java.util.Collections;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

public class ColorController implements DBController<List<Color>> {

    private final SessionFactory sessionFactory;
    private List<Color> cashedList = null;

    public ColorController(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Color> getData() {
        if (cashedList == null) {
            try (Session session = sessionFactory.openSession()) {
                Query<Color> listColors = session.createQuery("from Color", Color.class);
                cashedList = Collections.unmodifiableList(listColors.list());
            }
        }
        return cashedList;
    }
}
