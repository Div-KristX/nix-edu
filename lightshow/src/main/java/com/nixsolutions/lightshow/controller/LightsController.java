package com.nixsolutions.lightshow.controller;

import com.nixsolutions.lightshow.model.Light;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

public class LightsController implements DBController<List<Light>> {

    private final SessionFactory sessionFactory;

    public LightsController(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Light> getData() {
        List<Light> lights;
        try (Session session = sessionFactory.openSession()) {
            Query<Light> lightList = session.createQuery("from Light", Light.class);
            lights = lightList.list();
        }
        return lights;
    }
}
