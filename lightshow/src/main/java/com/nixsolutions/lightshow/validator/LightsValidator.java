package com.nixsolutions.lightshow.validator;

import com.nixsolutions.lightshow.exceptions.LightshowValidatorExeption;
import com.nixsolutions.lightshow.model.Light;
import java.util.List;

public class LightsValidator implements Validator {

    private final List<Light> dbLight;

    private final String label;

    public LightsValidator(List<Light> dbLight, String label) {
        this.dbLight = dbLight;
        this.label = label;
    }

    @Override
    public boolean validate() throws LightshowValidatorExeption {
        return dbLight.stream()
            .filter(l -> l.getLabel().equals(label))
            .findFirst()
            .orElseThrow(() -> new LightshowValidatorExeption("Light '" + label + "' does not exists"))
            .getEnabled();
    }
}
