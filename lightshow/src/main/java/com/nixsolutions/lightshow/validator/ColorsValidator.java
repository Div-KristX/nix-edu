package com.nixsolutions.lightshow.validator;

import com.nixsolutions.lightshow.exceptions.LightshowValidatorExeption;
import com.nixsolutions.lightshow.model.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorsValidator implements Validator {

    private final List<Color> dbColors;

    private final List<Color> inputColors;

    public ColorsValidator(List<Color> dbColors, List<Color> inputColors) {
        this.dbColors = dbColors;
        this.inputColors = inputColors;
    }

    @Override
    public boolean validate() throws LightshowValidatorExeption {
        List<Color> colorsCopy = new ArrayList<>(inputColors);
        if (colorsCopy.retainAll(dbColors)) {
            throw new LightshowValidatorExeption("Colors does not match");
        } else if (inputColors.stream().allMatch(color -> inputColors.get(0).equals(color))) {
            throw new LightshowValidatorExeption("Input colors are the same");
        }
        return true;
    }
}
