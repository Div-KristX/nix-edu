package com.nixsolutions.lightshow.validator;

import com.nixsolutions.lightshow.exceptions.LightshowValidatorExeption;

public interface Validator {

    boolean validate() throws LightshowValidatorExeption;
}
