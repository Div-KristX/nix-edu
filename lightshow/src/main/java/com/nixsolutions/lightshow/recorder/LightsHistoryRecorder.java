package com.nixsolutions.lightshow.recorder;

import com.nixsolutions.lightshow.model.Color;
import com.nixsolutions.lightshow.model.ColorHistoryRecord;
import com.nixsolutions.lightshow.model.Light;
import java.sql.Timestamp;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightsHistoryRecorder implements Recorder {

    private static final Logger log = LoggerFactory.getLogger(LightUpdateRecorder.class);
    private final Light light;

    private final Color previousColor;

    private final Color newColor;

    private final SessionFactory sessionFactory;

    private final Timestamp timestamp;


    public LightsHistoryRecorder(Light light, Color previousColor, Color newColor,
        Timestamp timestamp,
        SessionFactory sessionFactory) {
        this.light = light;
        this.previousColor = previousColor;
        this.newColor = newColor;
        this.timestamp = timestamp;
        this.sessionFactory = sessionFactory;
    }


    @Override
    public boolean record() {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            ColorHistoryRecord record = new ColorHistoryRecord();
            record.setLight(light);
            record.setOldColor(previousColor);
            record.setNewColor(newColor);
            record.setTimestamp(timestamp);
            session.persist(record);
            log.info("Record changes with Light, label - '{}'", light.getLabel());
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw new RuntimeException(e);
        }
        return true;
    }
}
