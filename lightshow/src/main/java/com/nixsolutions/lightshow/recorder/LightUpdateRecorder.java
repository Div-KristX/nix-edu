package com.nixsolutions.lightshow.recorder;

import com.nixsolutions.lightshow.exceptions.LightshowRecordException;
import com.nixsolutions.lightshow.model.Light;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightUpdateRecorder implements Recorder {

    private static final Logger log = LoggerFactory.getLogger(LightUpdateRecorder.class);
    private final Light light;

    private final SessionFactory sessionFactory;
    private final boolean addMode;

    public LightUpdateRecorder(Light light, SessionFactory sessionFactory, boolean addMode) {
        this.light = light;
        this.sessionFactory = sessionFactory;
        this.addMode = addMode;
    }

    @Override
    public boolean record() throws LightshowRecordException {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            if (addMode) {
                session.persist(light);
                log.info("Adding new light with label '{}', color {}",
                    light.getLabel(), light.getCurrentColor().getName());
            } else {
                session.merge(light);
                log.info("Updating Light with label '{}'", light.getLabel());
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            log.error(e.getMessage());
            throw new LightshowRecordException(e);
        }
        return true;
    }
}
