package com.nixsolutions.lightshow.recorder;

import com.nixsolutions.lightshow.exceptions.LightshowRecordException;

public interface Recorder {
    boolean record() throws LightshowRecordException;
}
