package com.nixsolutions.lightshow.exceptions;

public class LightshowException extends Exception {

    public LightshowException() {
    }

    public LightshowException(String message) {
        super(message);
    }

    public LightshowException(String message, Throwable cause) {
        super(message, cause);
    }

    public LightshowException(Throwable cause) {
        super(cause);
    }
}
