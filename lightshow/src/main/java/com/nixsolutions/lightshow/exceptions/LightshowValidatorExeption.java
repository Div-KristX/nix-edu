package com.nixsolutions.lightshow.exceptions;

public class LightshowValidatorExeption extends Exception{

    public LightshowValidatorExeption() {
    }

    public LightshowValidatorExeption(String message) {
        super(message);
    }

    public LightshowValidatorExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public LightshowValidatorExeption(Throwable cause) {
        super(cause);
    }
}
