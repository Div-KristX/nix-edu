package com.nixsolutions.lightshow.exceptions;

public class LightshowRecordException extends Exception {

    public LightshowRecordException() {
    }

    public LightshowRecordException(String message) {
        super(message);
    }

    public LightshowRecordException(String message, Throwable cause) {
        super(message, cause);
    }

    public LightshowRecordException(Throwable cause) {
        super(cause);
    }
}
