package com.nixsolutions.lightshow;

import com.nixsolutions.lightshow.cli.UserInterface;
import com.nixsolutions.lightshow.exceptions.LightshowException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class LightShowApp {

    public static void main(String[] args) throws LightshowException {
        try (StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure()
            .build();
            SessionFactory factory = new MetadataSources(registry).buildMetadata()
                .buildSessionFactory()) {
            UserInterface userInterface = new UserInterface(factory);
            userInterface.run();
        }
    }
}
