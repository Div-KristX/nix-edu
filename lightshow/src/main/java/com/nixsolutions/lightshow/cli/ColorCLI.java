package com.nixsolutions.lightshow.cli;

import com.nixsolutions.lightshow.exceptions.LightshowValidatorExeption;
import com.nixsolutions.lightshow.model.Color;
import com.nixsolutions.lightshow.validator.ColorsValidator;
import com.nixsolutions.lightshow.validator.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ColorCLI implements InteractiveCLI<List<Color>> {

    private static final Logger log = LoggerFactory.getLogger(ColorCLI.class);
    private final List<Color> dbColors;

    public ColorCLI (List<Color> dbColors) {
        this.dbColors = dbColors;
    }

    private static void setIdToColors(List<Color> colors, List<Color> dbColors) {
        Map<String, Long> dbColorsMapping = dbColors.stream()
            .collect(Collectors.toMap(Color::getName, Color::getId));
        colors.forEach(color -> color.setId(dbColorsMapping.get(color.getName())));
    }

    @Override
    public List<Color> run() {
        Scanner scanner = new Scanner(System.in);
        log.info("Collecting info about input colors");
        System.out.println("Available colors: ");
        dbColors.forEach(color -> System.out.println(" - " + color.getName()));

        System.out.println("Write colors {color, color1, color2, ...}");
        List<String> inputText = List.of(scanner.nextLine().trim().split(", "));

        List<Color> colors = new ArrayList<>();
        for (String s : inputText) {
            Color color = new Color();
            color.setName(s);
            colors.add(color);
        }
        Validator validator = new ColorsValidator(dbColors, colors);
        try {
            validator.validate();
            log.info("Colors are correct");
        } catch (LightshowValidatorExeption e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
        setIdToColors(colors, dbColors);
        log.info("Setting ids to the input colors");
        return colors;
    }
}
