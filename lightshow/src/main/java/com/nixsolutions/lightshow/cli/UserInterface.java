package com.nixsolutions.lightshow.cli;

import com.nixsolutions.lightshow.controller.ColorController;
import com.nixsolutions.lightshow.controller.DBController;
import com.nixsolutions.lightshow.controller.LightsController;
import com.nixsolutions.lightshow.model.Color;
import com.nixsolutions.lightshow.model.Light;
import com.nixsolutions.lightshow.simulator.LightshowSimulator;
import com.nixsolutions.lightshow.simulator.Simulator;
import java.util.List;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInterface  {

    private static final Logger log = LoggerFactory.getLogger(UserInterface.class);
    private final SessionFactory sessionFactory;

    public UserInterface(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void run() {

        DBController<List<Light>> dbLights = new LightsController(sessionFactory);

        log.info("Collecting Lights from DB");
        InteractiveCLI<Light> lightsCLI = new LightsCLI(dbLights.getData());

        Light light = lightsCLI.run();

        DBController<List<Color>> dbColors = new ColorController(sessionFactory);

        log.info("Collecting colors from DB");
        InteractiveCLI<List<Color>> colorCLI = new ColorCLI(dbColors.getData());

        List<Color> colors = colorCLI.run();

        log.info("Collecting info for the simulator`s properties");
        InteractiveCLI<Integer[]> simulatorProperties = new SimulatorCLI();

        Integer[] properties = simulatorProperties.run();

        Simulator simulator = new LightshowSimulator(sessionFactory, colors, light, properties[0],
            properties[1]);
        simulator.simulate();
    }
}
