package com.nixsolutions.lightshow.cli;

import java.util.Scanner;

public class SimulatorCLI implements InteractiveCLI<Integer[]> {

    @Override
    public Integer[] run() {
        Integer[] properties = new Integer[2];
        Scanner scanner = new Scanner(System.in);

        System.out.println("Interval (s) - ");
        properties[0] = scanner.nextInt();
        if (properties[0] < 1) {
            throw new RuntimeException("Invalid value of the interval " + properties[0]);
        }
        System.out.println("Count of switches - ");
        properties[1] = scanner.nextInt();
        if (properties[1] < 1) {
            throw new RuntimeException("Invalid value of the switches count " + properties[1]);
        }
        return properties;
    }
}
