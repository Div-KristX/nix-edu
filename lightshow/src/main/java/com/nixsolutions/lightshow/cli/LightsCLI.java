package com.nixsolutions.lightshow.cli;

import com.nixsolutions.lightshow.exceptions.LightshowException;
import com.nixsolutions.lightshow.exceptions.LightshowValidatorExeption;
import com.nixsolutions.lightshow.model.Light;
import com.nixsolutions.lightshow.validator.LightsValidator;
import com.nixsolutions.lightshow.validator.Validator;
import java.util.List;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LightsCLI implements InteractiveCLI<Light> {

    private static final Logger log = LoggerFactory.getLogger(LightsCLI.class);
    private final List<Light> dbLights;

    public LightsCLI(List<Light> dbLights) {
        this.dbLights = dbLights;
    }

    @Override
    public Light run() {
        Scanner scanner = new Scanner(System.in);

        log.info("Collect info about Light`s label");

        System.out.println("Available lights");

        dbLights.forEach(light -> System.out.println(
            " - " + light.getLabel() + " (" + light.getCurrentColor().getName() + ")"));

        System.out.println("Light`s label");

        String label = scanner.nextLine().trim();

        Validator validator = new LightsValidator(dbLights, label);

        Light light;
        try {
            if (validator.validate()) {
                log.error("{} - was enabled", label);
                throw new RuntimeException(new LightshowException("The light is enabled"));
            } else {
                log.info("{} - was found", label);
                light = dbLights.stream()
                    .filter(l -> l.getLabel().equals(label))
                    .findFirst()
                    .get();
            }
        } catch (LightshowValidatorExeption e) {
            log.error(e.getMessage());
            log.info("Light '{}' is creating", label);

            light = new Light();
            light.setLabel(label);
            light.setEnabled(null);

            return light;
        }
        return light;
    }
}
