package com.nixsolutions.lightshow.cli;

public interface InteractiveCLI<T> {

    T run();
}
