package com.nixsolutions.lightshow.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "lights")
public class Light {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "label", unique = true)
    private String label;

    @ManyToOne
    @JoinColumn(name = "color_id", nullable = false)
    private Color currentColor;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @OneToMany(mappedBy = "light", fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    private List<ColorHistoryRecord> records;

    public List<ColorHistoryRecord> getRecords() {
        return records;
    }

    public void setRecords(List<ColorHistoryRecord> records) {
        this.records = records;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Light light = (Light) o;
        return label.equals(light.label) && currentColor.equals(light.currentColor)
            && enabled.equals(
            light.enabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, currentColor, enabled);
    }
}
