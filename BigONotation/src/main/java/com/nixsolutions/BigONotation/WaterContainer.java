/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.BigONotation;

/**
 * The {@code  WaterContainer} class
 *
 * @author Krasnov Vladyslav
 */
public class WaterContainer {

    /**
     * The method finds the biggest amount of water inside two lines that together with the x-axis
     * form a container.
     *
     * @param height input array
     * @return the maximum amount of water a container can store.
     */
    public static int maxArea(int[] height) {
        int maxWater = 0;
        for (int rightColumn = 0, leftColumn = height.length - 1; rightColumn < leftColumn; ) {
            maxWater = Math.max(maxWater,
                (Math.min(height[rightColumn], height[leftColumn]) * (leftColumn - rightColumn)));
            if (height[rightColumn] < height[leftColumn]) {
                rightColumn++;
            } else {
                leftColumn--;
            }
        }
        return maxWater;
    }
}
