/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.BigONotation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The {@code  WaterContainerTest} class
 *
 * @author Krasnov Vladyslav
 */
public class WaterContainerTest {

    private int[] arrayOfEightColumns;
    private int[] arrayOfTwoColumns;

    @Before
    public void setUp() throws Exception {
        arrayOfEightColumns = new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7};
        arrayOfTwoColumns = new int[]{1, 1};

    }

    @After
    public void tearDown() throws Exception {
        arrayOfEightColumns = null;
        arrayOfTwoColumns = null;
    }

    @Test
    public void maxWaterInContainerFor8ColumnsIs49() {
        Assert.assertEquals(49, WaterContainer.maxArea(arrayOfEightColumns));
    }

    @Test
    public void maxWaterInContainerFor2ColumnsIs2() {
        Assert.assertEquals(1, WaterContainer.maxArea(arrayOfTwoColumns));
    }
}