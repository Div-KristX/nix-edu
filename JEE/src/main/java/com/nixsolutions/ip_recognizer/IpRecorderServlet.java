package com.nixsolutions.ip_recognizer;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "ip-recorder", urlPatterns = "/history")
public class IpRecorderServlet extends HttpServlet {

    @Serial
    private static final long serialVersionUID = -3829467188201322095L;

    private static final Logger log = LoggerFactory.getLogger(IpRecorderServlet.class);

    private final Set<String> ipAddressesAgents = ConcurrentHashMap.newKeySet();

    @Override
    public void init() throws ServletException {
        log.info("{} initialized", getServletName());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter responseBody = resp.getWriter();
        resp.setContentType("text/html");
        responseBody.println("<h2 align = \"center\"> Unique Ips visitors </h2>");
        String incomingRequest = req.getRemoteAddr() + " :: " + req.getHeader("user-agent");
        req.getRequestURL();
        ipAddressesAgents.add(incomingRequest);
        for (String singleIpAndAgent : ipAddressesAgents) {
            if (singleIpAndAgent.equals(incomingRequest)) {
                responseBody.println("<b><p align = \"center\">" + incomingRequest + " </p></b>");
            } else {
                responseBody.println("<p align = \"center\">" + singleIpAndAgent + " </p>");
            }
        }
    }


    @Override
    public void destroy() {
        log.info("{} destroyed", getServletName());
    }
}
