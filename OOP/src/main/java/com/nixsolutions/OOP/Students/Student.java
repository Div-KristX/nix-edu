/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.Students;

import java.util.Objects;

/**
 * The {@code  Student} class
 *
 * @author Krasnov Vladyslav
 */
public class Student {

    private final String name;
    private final int age;

    Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * The method returns an age of an instance.
     *
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * The method returns a name of an instance.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * The method returns string format of an instance.
     *
     * @return String format
     */
    public String toString() {
        return name + " " + age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return age == student.age && name.equals(student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
