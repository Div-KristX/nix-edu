/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.Students;

import java.util.Objects;

/**
 * The {@code  ContractStudent} class
 *
 * @author Krasnov Vladyslav
 */
public class ContractStudent extends Student {

    private final double educationCost;

    ContractStudent(String name, int age, double educationCost) {
        super(name, age);
        this.educationCost = educationCost;
    }

    /**
     * The method returns string format of an instance.
     *
     * @return String format
     */
    public String toString() {
        return getName() + " " + getAge() + " " + educationCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ContractStudent that = (ContractStudent) o;
        return Double.compare(that.educationCost, educationCost) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), educationCost);
    }

}
