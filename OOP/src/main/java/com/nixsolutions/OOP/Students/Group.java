/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.Students;

import java.util.Arrays;

/**
 * The {@code  Group} class
 *
 * @author Krasnov Vladyslav
 */
public class Group {

    private final Student[] students;

    Group(Student[] students) {
        this.students = students;
    }

    /**
     * The method writes only contract students from the Group, as " NAME + AGE + EDUCATION COST"
     *
     * @return Contract students from the Group
     */
    public String getContractStudents() {
        StringBuilder contractStudents = new StringBuilder();
        for (Student student : students) {
            if (student.getClass().equals(ContractStudent.class)) {
                contractStudents.append(student).append("\n");
            }
        }
        return contractStudents.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return Arrays.equals(students, group.students);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(students);
    }
}
