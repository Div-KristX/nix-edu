/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

/**
 * The {@code  Substance} interface
 *
 * @author Krasnov Vladyslav
 */
public interface Substance {

    /**
     * The method increase or decrease the temperature of an Instance, and returns the state of it.
     *
     * @param t Temperature to increase or decrease
     * @return State of the Instance
     */
    State heatUp(double t);

    /**
     * The method returns Instance`s temperature.
     *
     * @return Instance`s temperature
     */
    double getTemperature();
}
