/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

import java.util.Objects;

/**
 * The {@code  Water} class
 *
 * @author Krasnov Vladyslav
 */
public class Water implements Substance {

    private double temperature = 20;

    /**
     * The method increases or decreases the temperature of an Instance, and returns the state of
     * it.
     *
     * @param t Temperature to increase or decrease
     * @return State of the Instance
     */
    @Override
    public State heatUp(double t) {
        temperature += t;
        if (temperature >= 100) {
            return State.GAS;
        } else if (temperature <= 0) {
            return State.SOLID;
        } else {
            return State.LIQUID;
        }
    }

    /**
     * The method returns Instance`s temperature.
     *
     * @return Instance`s temperature
     */
    @Override
    public double getTemperature() {
        return temperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Water water = (Water) o;
        return Double.compare(water.temperature, temperature) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature);
    }
}
