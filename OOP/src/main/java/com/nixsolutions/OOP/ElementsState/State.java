/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

/**
 * The {@code  State} enum
 *
 * @author Krasnov Vladyslav
 */
public enum State {
    SOLID,
    LIQUID,
    GAS
}
