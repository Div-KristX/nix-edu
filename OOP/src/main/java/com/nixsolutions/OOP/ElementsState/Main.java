/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

/**
 * The {@code  Main} class to test  {@code  ElementsState} package, with an input information from
 * the console.
 *
 * @author Krasnov Vladyslav
 */
public class Main {

    public static void main(String[] args) {
        ElementController elementController = new ElementController();
        elementController.createElement();
        elementController.changingTemperature();
    }

}
