/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

import java.util.Objects;

/**
 * The {@code  Oxygen} class
 *
 * @author Krasnov Vladyslav
 */
public class Oxygen implements Substance {

    private double temperature = 20;

    /**
     * The method increases or decreases the temperature of an Instance, and returns the state of it.
     *
     * @param t Temperature to increase or decrease
     * @return State of the Instance
     */
    @Override
    public State heatUp(double t) {
        temperature += t;
        if (temperature <= -222.65) {
            return State.SOLID;
        } else if (temperature <= -183) {
            return State.LIQUID;
        } else {
            return State.GAS;
        }
    }

    /**
     * The method returns Instance`s temperature.
     *
     * @return Instance`s temperature
     */
    @Override
    public double getTemperature() {
        return temperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Oxygen oxygen = (Oxygen) o;
        return Double.compare(oxygen.temperature, temperature) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature);
    }
}
