/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

import java.util.Objects;
import java.util.Scanner;

/**
 * The {@code  ElementController} class
 *
 * @author Krasnov Vladyslav
 */
public class ElementController {

    private Substance substance;

    /**
     * The method returns an instance of a chosen element.
     */
    public void createElement() {
        int choice = -1;
        while (choice < 0 || choice > 2) {
            System.out.println("0 - Water  1 - Iron  2 - Oxygen");
            Scanner inputNumber = new Scanner(System.in);
            choice = inputNumber.nextInt();
        }
        if (choice == 0) {
            this.substance = new Water();
        } else if (choice == 1) {
            this.substance = new Iron();
        } else {
            this.substance = new Oxygen();
        }
    }

    /**
     * The method increases or decreases the value of a temperature of an instance.
     */
    public void changingTemperature() {
        double addTemperature = -1;
        while (addTemperature != 0) {
            System.out.println("Temperature to add: ");
            Scanner inputNumber = new Scanner(System.in);
            addTemperature = inputNumber.nextInt();
            System.out.println(substance.heatUp(addTemperature));
            System.out.println(substance.getTemperature());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ElementController that = (ElementController) o;
        return Objects.equals(substance, that.substance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(substance);
    }
}
