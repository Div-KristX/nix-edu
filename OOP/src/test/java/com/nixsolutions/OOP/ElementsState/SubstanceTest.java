/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.ElementsState;

import java.util.Scanner;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The {@code  SubstanceTest} class
 *
 * @author Krasnov Vladyslav
 */
public class SubstanceTest {

    private static final Logger LOGGER_4_J = LogManager.getLogger(SubstanceTest.class);
    private Substance substance;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        substance = null;
    }

    @Test
    public void waterStates() {
        substance = createElement();
        changingTemperature();
    }

    /**
     * The method returns an instance of a chosen element.
     *
     * @return Element`s instance
     */
    private Substance createElement() {
        int choice = -1;
        while (choice < 0 || choice > 2) {
            LOGGER_4_J.info("0 - Water  1 - Iron  2 - Oxygen");
            Scanner inputNumber = new Scanner(System.in);
            choice = inputNumber.nextInt();
        }
        if (choice == 0) {
            return new Water();
        } else if (choice == 1) {
            return new Iron();
        } else {
            return new Oxygen();
        }
    }

    /**
     * The method increases or decreases the value of a temperature of an instance.
     */
    private void changingTemperature() {
        double addTemperature = -1;
        while (addTemperature != 0) {
            LOGGER_4_J.info("Temperature to add: ");
            Scanner inputNumber = new Scanner(System.in);
            addTemperature = inputNumber.nextInt();
            LOGGER_4_J.info(substance.heatUp(addTemperature));
            LOGGER_4_J.info(substance.getTemperature());
        }
    }
}