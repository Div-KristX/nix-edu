/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.OOP.Students;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * The {@code  GroupTest} class
 *
 * @author Krasnov Vladyslav
 */
public class GroupTest {

    private static final Logger LOGGER_4_J = LogManager.getLogger(GroupTest.class);
    private Group group;
    private Student[] students;
    private String contractStudents;

    @Before
    public void setUp() throws Exception {
        students = new Student[5];
        students[0] = new Student("John", 19);
        students[1] = new ContractStudent("JoJo", 20, 18000D);
        students[2] = new Student("Abdul", 21);
        students[3] = new ContractStudent("Noel", 20, 15000D);
        students[4] = new Student("Anna", 18);
        group = new Group(students);
        contractStudents = "JoJo 20 18000.0\n" + "Noel 20 15000.0\n";
    }

    @After
    public void tearDown() throws Exception {
        group = null;
        students = null;
        contractStudents = null;
    }

    @Test
    public void JoJoAndNoelAreContract() {
        Assert.assertEquals(contractStudents, group.getContractStudents());
    }

    @Test
    public void allContractStudents() {
        LOGGER_4_J.info(group.getContractStudents());
    }
}