package com.nixsolutions.dining_philosophers;


public class PhilosophersTable {

    private Runnable pickLeftFork;
    private Runnable pickRightFork;
    private Runnable eat;
    private Runnable putLeftFork;
    private Runnable putRightFork;

    public PhilosophersTable(Runnable pickLeftFork, Runnable pickRightFork, Runnable eat,
        Runnable putLeftFork, Runnable putRightFork) {
        this.pickLeftFork = pickLeftFork;
        this.pickRightFork = pickRightFork;
        this.eat = eat;
        this.putLeftFork = putLeftFork;
        this.putRightFork = putRightFork;
    }

    public PhilosophersTable() {

    }

    public void wantsToEat(int philosopher, Runnable pickLeftFork, Runnable pickRightFork,
        Runnable eat, Runnable putLeftFork, Runnable putRightFork) throws InterruptedException {
        tryToEat(philosopher, pickLeftFork, pickRightFork, eat, putLeftFork, putRightFork);
    }

    public void wantsToEat(int philosopher) {
        tryToEat(philosopher, pickLeftFork, pickRightFork, eat, putLeftFork, putRightFork);
    }

    private void tryToEat(int philosopher, Runnable pickLeftFork, Runnable pickRightFork,
        Runnable eat, Runnable putLeftFork, Runnable putRightFork) {
        try {
            pickLeftFork.run();
            pickRightFork.run();
            System.out.println(philosopher + " is starting to eat");
            eat.run();
            putLeftFork.run();
            putRightFork.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
