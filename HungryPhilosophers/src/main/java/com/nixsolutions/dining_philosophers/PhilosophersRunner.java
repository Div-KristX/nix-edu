package com.nixsolutions.dining_philosophers;

import com.nixsolutions.dining_philosophers.Philosopher.Fork;
import java.util.ArrayList;
import java.util.List;

public class PhilosophersRunner {

    public static void main(String[] args) throws InterruptedException {

        Runnable leftForkPicker = () -> System.out.println("Picking left fork");

        Runnable rightForkPicker = () -> System.out.println("Picking right fork");
        Runnable eat = () -> System.out.println("eating");
        Runnable leftForkDropper = () -> System.out.println("Dropping left fork");

        Runnable rightForkDropper = () -> System.out.println("Dropping right fork");

        PhilosophersTable table = new PhilosophersTable(leftForkPicker, rightForkPicker, eat,
            leftForkDropper, rightForkDropper);

        Fork MakiavelisFork = new Fork();
        Fork GandhisFork = new Fork();
        Fork GoethesFork = new Fork();
        Fork SunZisFork = new Fork();
        Fork GuGosFork = new Fork();

        Philosopher Makiaveli = new Philosopher(1, table, MakiavelisFork, GandhisFork);
        Philosopher Gandhi = new Philosopher(2, table, GandhisFork, GoethesFork);
        Philosopher Goethe = new Philosopher(3, table, GoethesFork, SunZisFork);
        Philosopher SunZi = new Philosopher(4, table, SunZisFork, GuGosFork);
        Philosopher GuGo = new Philosopher(5, table, GuGosFork, MakiavelisFork);

        List<Philosopher> philosophers = new ArrayList<>();
        philosophers.add(Makiaveli);
        philosophers.add(Gandhi);
        philosophers.add(Goethe);
        philosophers.add(SunZi);
        philosophers.add(GuGo);

        philosophers.forEach(Thread::start);
        for (Philosopher philosopher : philosophers) {
            philosopher.join();
        }
    }
}
