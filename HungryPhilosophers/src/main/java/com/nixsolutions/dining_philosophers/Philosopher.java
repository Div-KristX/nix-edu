package com.nixsolutions.dining_philosophers;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Philosopher extends Thread {

    private final int philosopherId;
    private final PhilosophersTable table;

    private final Fork ownFork;
    private final Fork leftHand;

    private final Lock rightForkLock;

    private final Lock leftForkLock;


    Philosopher(int philosopherId, PhilosophersTable table, Fork ownFork, Fork leftHand) {
        this.philosopherId = philosopherId;
        this.table = table;
        this.ownFork = ownFork;
        this.leftHand = leftHand;
        rightForkLock = new ReentrantLock();
        leftForkLock = new ReentrantLock();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Philosopher that = (Philosopher) o;
        return philosopherId == that.philosopherId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(philosopherId);
    }

    @Override
    public void run() {

        if (rightForkLock.tryLock()) {
            try {
                ownFork.setAvailable(false);
                if (leftForkLock.tryLock()) {
                    try {
                        leftHand.setAvailable(false);
                        table.wantsToEat(philosopherId);
                    } finally {
                        leftHand.setAvailable(true);
                        leftForkLock.unlock();
                    }
                }
            } finally {
                ownFork.setAvailable(true);
                rightForkLock.unlock();
            }
        }
    }


    public static class Fork {

        private volatile boolean available = true;

        public boolean isAvailable() {
            return available;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }
    }
}

