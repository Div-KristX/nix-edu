/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.csv;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code  CSVMapper} class
 *
 * @author Krasnov Vladyslav
 */
public class CSVMapper {

    /**
     * Creates a list of objects with field values from CSVTable.
     *
     * @param table      Input CSV table
     * @param resultType output Class
     * @return List of the objects
     */
    public <T> List<T> map(CSVTable table, Class<T> resultType) {
        List<T> instances = new ArrayList<>(table.getCapacity());
        for (int i = 0; i < table.getCapacity(); i++) {
            try {
                Constructor<T> constructor = resultType.getConstructor();
                T providedType = constructor.newInstance();
                for (Field field : resultType.getDeclaredFields()) {
                    field.setAccessible(true);
                    TableFields key = field.getAnnotation(TableFields.class);
                    String value = table.get(i, key.value());
                    if (value != null) {
                        Class<?> type = field.getType();
                        if (type == String.class) {
                            field.set(providedType, value);
                        } else if (type.isEnum()) {
                            field.set(providedType, Enum.valueOf((Class<Enum>) type, value));
                        } else if (type == int.class || type == Integer.class) {
                            field.setInt(providedType, Integer.parseInt(value));
                        } else if (type == long.class || type == Long.class) {
                            field.setLong(providedType, Long.parseLong(value));
                        } else if (type == double.class || type == Double.class) {
                            field.setDouble(providedType, Double.parseDouble(value));
                        } else if (type == boolean.class || type == Boolean.class) {
                            field.setBoolean(providedType, Boolean.parseBoolean(value));
                        } else {
                            throw new UnsupportedOperationException("Unsupported field type (" +
                                    type.getName() + ") is required for field " +
                                    field.getName());
                        }
                    }
                }
                instances.add(providedType);
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                     InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return instances;
    }
}
