package com.nixsolutions.csv.io;

import com.nixsolutions.csv.CSVTable;


/**
 * The {@code  CSVParser} interface
 *
 * @author Krasnov Vladyslav
 */
public interface CSVParser<T> {
    CSVTable parse(T source);
}
