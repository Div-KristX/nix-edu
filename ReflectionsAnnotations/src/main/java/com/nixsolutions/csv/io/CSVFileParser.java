/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.csv.io;

import com.nixsolutions.csv.CSVTable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;

/**
 * The {@code  CSVParser} class
 *
 * @author Krasnov Vladyslav
 */
public class CSVFileParser implements CSVParser<Path> {

    @Override
    public CSVTable parse(Path path) {
        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            String line;
            CSVTable table = new CSVTable();
            while ((line = reader.readLine()) != null) {
                table.add(line.split(","));
            }
            return table;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
