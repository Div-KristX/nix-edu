/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.csv;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@code  TableFields} Annotation
 *
 * @author Krasnov Vladyslav
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TableFields {
    String value();
}
