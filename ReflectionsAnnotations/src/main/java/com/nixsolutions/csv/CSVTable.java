/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.csv;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The {@code  CSVTable} class
 *
 * @author Krasnov Vladyslav
 */
public class CSVTable {

    private final Map<String, List<CSVCell>> table;
    private int capacity = 0;

    private int columns = 0;

    public CSVTable() {
        this.table = new LinkedHashMap<>();
    }

    /**
     * Adds for the first time columns to the table, then every "add" call will add new rows to the table.
     *
     * @param row Input values for the table
     */
    public void add(String[] row) {
        if (row == null) {
            throw new IllegalArgumentException("Null input");
        }
        if (table.isEmpty()) {
            for (String s : row) {
                table.put(s, new ArrayList<>());
            }
            columns = table.size();
        } else {
            int count = 0;
            for (Map.Entry<String, List<CSVCell>> a : table.entrySet()) {
                List<CSVCell> lastValue = a.getValue();
                lastValue.add(new CSVCell(row[count]));
                a.setValue(lastValue);
                count++;
            }
            capacity++;
        }
    }

    /**
     * Returns the count of rows.
     *
     * @return Table capacity.
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns table`s cell by column`s name and row number.
     *
     * @return Table`s cell.
     */
    public String get(int row, String column) {
        if (row > capacity - 1) {
            throw new IllegalArgumentException("row is greater than table rows count");
        }
        return table.get(column).get(row).getObject();
    }

    /**
     * Returns table`s cell by column and row number.
     *
     * @return Table`s cell.
     */
    public String get(int row, int column) {
        if (row > capacity - 1 || column > columns - 1) {
            throw new IllegalArgumentException("Invalid row-column values");
        }
        String columnName = "";
        int count = 0;
        for (String key : table.keySet()) {
            if (count == column) {
                columnName = key;
            }
            count++;
        }
        return table.get(columnName).get(row).getObject();
    }

    /**
     * Returns table`s columns.
     *
     * @return Table`s columns.
     */
    public List<String> columns() {
        return table.keySet().stream().toList();
    }

    @Override
    public String toString() {
        return "CSVTable{" + "table=" + table + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CSVTable table1 = (CSVTable) o;
        return Objects.equals(table, table1.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table);
    }

    private record CSVCell(String object) {

        public String getObject() {
            return object;
        }

        @Override
        public String toString() {
            return "CSVCell{" + "object='" + object + '\'' + '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            CSVCell csvRow = (CSVCell) o;
            return Objects.equals(object, csvRow.object);
        }

        @Override
        public int hashCode() {
            return Objects.hash(object);
        }
    }
}
