package com.nixsolutions.csv;

import java.nio.file.Path;
import java.util.List;

import com.nixsolutions.csv.io.CSVFileParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CSVTableTest {

    private CSVTable table;
    private CSVFileParser parser;

    @BeforeEach
    void setUp() {
        parser = new CSVFileParser();
        table = new CSVTable();
    }

    @AfterEach
    void tearDown() {
        parser = null;
        table = null;
    }

    @Test
    public void parserTest() {
        Path path = Path.of("resources/example.cvs");
        table = new CSVFileParser().parse(path);
        Assertions.assertEquals("The World", (table.get(1, "stand")));
        Assertions.assertEquals(3414, Integer.valueOf(table.get(0, 2)));
        Assertions.assertEquals(List.of("name", "stand", "power"), table.columns());
    }

    @Test
    public void CreationTest() {
        Path path = Path.of("resources/example.cvs");
        table = parser.parse(path);
        List<StarDustCrusaders> stars = new CSVMapper().map(table, StarDustCrusaders.class);
        Assertions.assertEquals("JoJo", stars.get(0).getUserName());
        Assertions.assertEquals("Star Platinum", stars.get(0).getStandName());
        Assertions.assertEquals(3414, stars.get(1).getPower());
    }
}