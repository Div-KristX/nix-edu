package com.nixsolutions.csv;

import java.util.Objects;

public class StarDustCrusaders {

    @TableFields("name")
    private String userName;
    @TableFields("stand")
    private String standName;
    @TableFields("power")
    private int power;

    public StarDustCrusaders(String userName, String standName, int power) {
        this.userName = userName;
        this.standName = standName;
        this.power = power;
    }

    public StarDustCrusaders() {
    }

    public String getUserName() {
        return userName;
    }

    public String getStandName() {
        return standName;
    }

    public int getPower() {
        return power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StarDustCrusaders that = (StarDustCrusaders) o;
        return power == that.power && Objects.equals(userName, that.userName)
            && Objects.equals(standName, that.standName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, standName, power);
    }
}