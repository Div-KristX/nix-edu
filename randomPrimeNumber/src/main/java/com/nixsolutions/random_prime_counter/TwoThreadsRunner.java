package com.nixsolutions.random_prime_counter;

import com.nixsolutions.random_prime_counter.provider.IntegerGenerator;

public class TwoThreadsRunner {

    public static void main(String[] args) {

        System.out.println(Thread.currentThread().getName() + " Started");

        IntegerGenerator integerGenerator = new IntegerGenerator();

        PrimeNumberChecker integerChecker = new PrimeNumberChecker(integerGenerator);

        Thread generatorThread = new Thread(integerGenerator);

        Thread checkerThread = new Thread(integerChecker);

        generatorThread.start();
        checkerThread.start();

        try {
            checkerThread.join();
            generatorThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(Thread.currentThread().getName() + " Finished");
    }
}
