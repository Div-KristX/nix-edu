package com.nixsolutions.random_prime_counter.provider;

import java.util.Random;

public class IntegerGenerator implements Generator<Integer>, Runnable {

    private volatile Integer generatedValue = null;
    private boolean generate = true;


    @Override
    public Integer getRandomNumber() {
        generatedValue = new Random().nextInt(100);
        return generatedValue;
    }

    @Override
    public Integer getGeneratedValue() {
        return generatedValue;
    }


    public synchronized void stopGeneration() {
        generate = false;
    }

    @Override
    public void run() {
        System.out.println("Generator is running");
        while (generate) {
            getRandomNumber();
            synchronized (this) {
                notify();
            }
        }
        System.out.println("Generator is stopped");
    }
}
