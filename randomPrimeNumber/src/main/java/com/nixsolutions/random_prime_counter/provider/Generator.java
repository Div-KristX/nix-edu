package com.nixsolutions.random_prime_counter.provider;

public interface Generator<T extends Number> {

    T getRandomNumber();

    T getGeneratedValue();

}
