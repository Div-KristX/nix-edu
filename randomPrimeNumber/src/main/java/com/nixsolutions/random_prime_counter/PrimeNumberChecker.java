package com.nixsolutions.random_prime_counter;

import com.nixsolutions.random_prime_counter.provider.IntegerGenerator;
import java.util.stream.IntStream;

public class PrimeNumberChecker implements Runnable {

    private static final int PRIME_NUMBERS_ORDER = 10;
    private final IntegerGenerator generator;

    public PrimeNumberChecker(IntegerGenerator generator) {
        this.generator = generator;
    }

    private boolean isPrime(int number) {
        return IntStream.rangeClosed(2, number / 2).noneMatch(divider -> number % divider == 0);
    }

    @Override
    public void run() {
        System.out.println("Checker is running");
        int primeNumbersCount = 0;
        synchronized (generator) {
            if (generator.getGeneratedValue() == null) {
                try {
                    generator.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        while (primeNumbersCount < PRIME_NUMBERS_ORDER) {
            Integer value = generator.getGeneratedValue();
            if (isPrime(value)) {
                primeNumbersCount++;
                System.out.println(
                    "Count - " + primeNumbersCount + " " + value);
            }
        }
        generator.stopGeneration();
        System.out.println("Checker is stopped");
    }
}
