/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.collections;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * The {@code  ForwardLinkedListTest} class
 *
 * @author Krasnov Vladyslav
 */
public class ForwardLinkedListTest {

    private ForwardLinkedList<Integer> list;

    @Before
    public void setUp() {
        list = new ForwardLinkedList<>();
        list.add(4);
        list.add(1);
        list.add(5);
        list.add(4);
        list.add(1);
        list.add(5);
    }

    @After
    public void tearDown() {
        list = null;
    }

    @Test
    public void ConvertToArray() {
        System.out.println(Arrays.toString(list.toArray()));
    }

    @Test
    public void sortVariablesAndPrintWithIterator() {
        Comparator<Integer> comparator = Comparator.naturalOrder();
        list.sort(comparator);
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    public void IteratorRemovesAllOne() {
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == 1) {
                iterator.remove();
            }
        }
        System.out.println(list);
    }

    @Test
    public void ClearAll() {
        list.clear();
        System.out.println(list);
    }

    @Test
    public void EditSecondElementTo90() {
        list.set(1, 90);
        System.out.println(list);
    }

    @Test
    public void UnaryOperatorReplacesAllToOne() {
        UnaryOperator<Integer> func = new UnaryOperator<>() {
            @Override
            public Integer apply(Integer integer) {
                return 1;
            }
        };
        list.replaceAll(func);
        System.out.println(list);
    }

    @Test
    public void PredicateRemovesAllOne() {
        Predicate<Integer> func = new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer == 1;
            }
        };
        list.removeIf(func);
        System.out.println(list);
    }
}