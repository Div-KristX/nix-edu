/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.collections;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * The {@code  ForwardLinkedList} class
 *
 * @author Krasnov Vladyslav
 */
public class ForwardLinkedList<T> extends AbstractList<T> {

    private Node<T> last;
    private Node<T> first;
    private static final int MAX_SIZE = Integer.MAX_VALUE - 8;
    private int size;

    @Override
    public int size() {
        return size;
    }

    /**
     * {@inheritDoc}
     *
     * @implSpec This implementation returns {@code size() == 0}.
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public <t> t[] toArray(IntFunction<t[]> generator) {
        return toArray(generator.apply(0));
    }

    @Override
    public boolean add(T value) {
        if (size == MAX_SIZE) {
            return false;
        } else {
            addLast(value);
            size++;
            return true;
        }
    }

    /**
     * Adds new node to the last node
     */
    private void addLast(T value) {
        if (first == null) {
            first = new Node<>(value, null);
            last = first;
        } else {
            Node<T> node = new Node<>(value, null);
            last.setNext(node);
            last = node;
        }
    }

    @Override
    public T remove(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Index less then zero");
        } else if (index == size) {
            throw new IllegalArgumentException(
                "Index " + index + " of " + size + " - out of bounds");
        }
        Node<T> toRemove;
        if (index == 0) {
            toRemove = first;
            first = first.next;
        } else {
            Node<T> previous = getNode(index - 1);
            toRemove = previous.next;
            Node<T> acrossRemoved = toRemove.next;
            previous.setNext(acrossRemoved);
        }
        size--;
        return toRemove.value;
    }

    @Override
    public T set(int index, T value) {
        getNode(index).value = value;
        return value;
    }

    @Override
    public void clear() {
        Node<T> iterator = first;
        Node<T> cleaner = first;
        for (int i = 1; i <= size; i++) {
            iterator = iterator.next;
            cleaner.next = null;
            cleaner.value = null;
            cleaner = iterator;
        }
        first.value = null;
        first = null;
        size = 0;
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        Objects.requireNonNull(filter);
        boolean removed = false;
        final Iterator<T> each = iterator();
        while (each.hasNext()) {
            if (filter.test(each.next())) {
                each.remove();
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        Objects.requireNonNull(operator);
        final ForwardListIterator li = new ForwardListIterator();
        while (li.hasNext()) {
            li.set(operator.apply(li.next()));
        }
    }

    @Override
    public void sort(Comparator<? super T> c) {
        Object[] list = this.toArray();
        Arrays.sort(list, (Comparator) c);
        clear();
        for (Object single : list) {
            add((T) single);
        }
    }

    @Override
    public T get(int index) {
        return getNode(index).value;
    }

    /**
     * Returns Node`s link
     */
    private Node<T> getNode(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Index less then zero");
        } else if (index == size) {
            throw new IllegalArgumentException(
                "Index " + index + " of " + size + " - out of bounds");
        }
        Node<T> node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        Objects.requireNonNull(action);
        for (T t : this) {
            action.accept(t);
        }
    }

    @Override
    public Spliterator<T> spliterator() {
        return super.spliterator();
    }

    @Override
    public Stream<T> stream() {
        return super.stream();
    }

    @Override
    public Stream<T> parallelStream() {
        return super.parallelStream();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ForwardLinkedList<?> that = (ForwardLinkedList<?>) o;
        return size == that.size && Objects.equals(last, that.last)
            && Objects.equals(first, that.first);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), last, first, size);
    }

    @Override
    public Iterator<T> iterator() {
        return new ForwardListIterator();
    }

    private static class Node<T> {

        private T value;
        private Node<T> next;


        Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

        /**
         * Sets a link to the next node
         *
         * @param next node
         */
        public void setNext(Node<T> next) {
            this.next = next;
        }
    }

    private class ForwardListIterator implements Iterator<T> {

        int cursor = 0;

        /**
         * Returns {@code true} if the iteration has more elements. (In other words, returns {@code
         * true} if {@link #next} would return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next() {
            T next = get(cursor);
            cursor++;
            return next;
        }

        @Override
        public void remove() {
            ForwardLinkedList.this.remove(cursor - 1);
            cursor--;
        }


        public T set(T value) {
            getNode(cursor - 1).value = value;
            return value;
        }
    }
}
