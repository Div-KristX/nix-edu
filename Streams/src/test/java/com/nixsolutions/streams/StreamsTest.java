/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.streams;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * The {@code  StreamsTest} class
 *
 * @author Krasnov Vladyslav
 */
public class StreamsTest {

    private DigitsInString digits;
    private Dates dates;
    private CommonBigDecimal commonBigDecimal;
    private List<String> strings;
    private List<LocalDateTime> datesList;
    private List<BigDecimal> numbers;

    @Before
    public void setUp() {
        digits = new DigitsInString();
        commonBigDecimal = new CommonBigDecimal();
        strings = new ArrayList<>();
        dates = new Dates();
        datesList = new ArrayList<>();
        numbers = new ArrayList<>();

        // for Dates
        datesList.add(LocalDateTime.of(2012, 3, 4, 5, 5));
        datesList.add(LocalDateTime.of(2012, 3, 4, 3, 5));
        datesList.add(LocalDateTime.of(2020, 3, 2, 3, 5));
        datesList.add(LocalDateTime.of(2022, 9, 4, 7, 7));
        datesList.add(LocalDateTime.of(2013, 2, 1, 19, 0));

        // for DigitsIngString
        strings.add("string 1 text");
        strings.add("2 string 3 text");
        strings.add("45");

        // for CommonBigDecimal
        numbers.add(new BigDecimal("111111111111111111111")); // 4 times
        numbers.add(new BigDecimal("111111111111111111111"));
        numbers.add(new BigDecimal("111111111111111111111"));
        numbers.add(new BigDecimal("111111111111111111111"));
        numbers.add(new BigDecimal("111111111111111111112")); // 2 times
        numbers.add(new BigDecimal("111111111111111111112"));
        numbers.add(new BigDecimal("111111111111111111113")); // 1 times
        numbers.add(new BigDecimal("111111111111111111114")); // 3 times
        numbers.add(new BigDecimal("111111111111111111114"));
        numbers.add(new BigDecimal("111111111111111111114"));
    }

    @After
    public void tearDown() {
        digits = null;
        commonBigDecimal = null;
        strings = null;
        dates = null;
        datesList = null;
        numbers = null;
    }

    @Test
    public void StringsContainFullNumberAs12345() {
        System.out.println(digits.onlyDigits(strings));
    }

    @Test
    public void MostCommonIs111111111111111111111() {
        System.out.println(commonBigDecimal.getMostCommon(numbers));
    }

    @Test
    public void DatesWithTimesFourDatesWithFiveTimes() {
        System.out.println(dates.sortDates(datesList));
    }
}