/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.streams;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The {@code  CommonBigDecimal} class
 *
 * @author Krasnov Vladyslav
 */
public class CommonBigDecimal {

    /**
     * Finds the most common BigDecimal number in a list of BigDecimal numbers.
     *
     * @param numbers input list of the numbers.
     * @return The most common BigDecimal number.
     */
    public BigDecimal getMostCommon(List<BigDecimal> numbers) {
        return numbers.stream()
            .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
            .entrySet()
            .stream()
            .max(Entry.comparingByValue())
            .orElseThrow(() -> new IllegalArgumentException("List is empty")).getKey();
    }
}
