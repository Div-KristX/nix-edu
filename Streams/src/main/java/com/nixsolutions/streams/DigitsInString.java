/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.streams;

import java.util.List;

/**
 * The {@code  DigitsInString} class
 *
 * @author Krasnov Vladyslav
 */
public class DigitsInString {

    /**
     * Finds the digits in a list of a strings and concat them to a one number.
     *
     * @param strings input list of the strings.
     * @return The number that was collected from the digits of the list of strings.
     */
    public int onlyDigits(List<String> strings) {
        return strings.stream()
            .flatMapToInt(String::codePoints)
            .filter(Character::isDigit)
            .map(e -> Character.digit(e, 10))
            .reduce((number, nextNumber) -> number * 10 + nextNumber)
            .orElseThrow(() -> new IllegalArgumentException("List is empty"));
    }
}
