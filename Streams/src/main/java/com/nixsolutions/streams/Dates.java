/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.streams;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@code  Dates} class
 *
 * @author Krasnov Vladyslav
 */
public class Dates {

    /**
     * Collects input list of LocalDateTime to pairs {date} - {time, time_2, ... time_n}.
     *
     * @param dates input list of dates.
     * @return Sorted map of the dates as {date} - {time, time_2, ... time_n}.
     */
    public Map<LocalDate, List<LocalTime>> sortDates(List<LocalDateTime> dates) {
        return dates.stream()
            .sorted(Comparator.comparing(LocalDateTime::toLocalDate))
            .collect(Collectors.groupingBy(LocalDateTime::toLocalDate, LinkedHashMap::new,
                Collectors.mapping(LocalDateTime::toLocalTime, Collectors.toCollection(
                    ArrayList::new))));
    }
}
