package com.nixsolutions.module2.LabirintParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class LabirintParser {

    public static List<String> getLabirint(Path path) {
        List<String> wholeLabirint = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                wholeLabirint.add(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wholeLabirint;
    }
}
