/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.module2;

import com.nixsolutions.module2.Ecxeptions.IncorrectPositionException;
import com.nixsolutions.module2.LabirintParser.LabirintParser;
import com.nixsolutions.module2.Validation.LabirintValidator;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

/**
 * The {@code  LabirintEscape} class
 *
 * @author Krasnov Vladyslav
 */
public class LabirintEscape {

    private final int startY;
    private final int startX;
    private final int exitY;
    private final int exitX;
    private final LabirintValidator validator;
    private final List<String> labirint;
    private final Queue<Node> nodes = new ArrayDeque<>();
    private Node smallestNode;

    public LabirintEscape(List<String> labirint, int startY, int startX, int exitY, int exitX) {
        this.startY = startY;
        this.startX = startX;
        this.exitY = exitY;
        this.exitX = exitX;
        validator = new LabirintValidator();
        this.labirint = labirint;
    }

    public void findWay() {
        validator.labirintIsCorrect(labirint);
        validator.startPointIsCorrect(labirint, startY, startX);
        validator.exitPointIsCorrect(labirint, exitY, exitX);
        int[][] convertedLabirint = convertTheLabirint();
        if (theMinimumRange(convertedLabirint, startY, startX, exitY, exitX) == -1) {
            throw new RuntimeException(
                new IncorrectPositionException("There is no way to the exit"));
        }
        printWay();
        nodes.clear();
    }

    private void printWay() {
        for (String text : paintWay()) {
            System.out.print(text);
        }
    }

    private String[] paintWay() {
        String[] waysInLabirint = labirint.toArray(new String[labirint.size()]);
        while (smallestNode.prev != null) {
            char[] old = waysInLabirint[smallestNode.posX].toCharArray();
            old[smallestNode.posY] = '#';
            String pointed = String.valueOf(old);
            waysInLabirint[smallestNode.posX] = pointed;
            smallestNode = smallestNode.prev;
            if (smallestNode.prev == null) {
                old = waysInLabirint[smallestNode.posX].toCharArray();
                old[smallestNode.posY] = '#';
                pointed = String.valueOf(old);
                waysInLabirint[smallestNode.posX] = pointed;
            }
        }
        return waysInLabirint;
    }


    private int[][] convertTheLabirint() {
        int[][] convertedLabirint = new int[labirint.size()][labirint.get(0).length()];
        for (int i = 0; i < labirint.size(); i++) {
            for (int j = 0; j < labirint.get(0).length(); j++) {
                if (labirint.get(i).charAt(j) == '+') {
                    convertedLabirint[i][j] = 1;
                } else {
                    convertedLabirint[i][j] = 0;
                }
            }
        }
        return convertedLabirint;
    }

    private static final int[] row = {-1, 0, 0, 1};
    private static final int[] col = {0, -1, 1, 0};

    private boolean correctMatrix(int[][] matrix, boolean[][] visited, int row, int col) {
        return (row >= 0) && (row < matrix.length) && (col >= 0) && (col < matrix[0].length)
            && matrix[row][col] == 1 && !visited[row][col];
    }

    private int theMinimumRange(int[][] matrix, int startX, int startY, int exitX,
        int exitY) {
        boolean[][] visited = new boolean[matrix.length][matrix[0].length];
        visited[startX][startY] = true;
        nodes.add(new Node(null, startX, startY, 0));
        int minDist = -1;
        while (!nodes.isEmpty()) {
            Node node = nodes.poll();
            startX = node.posX;
            startY = node.posY;
            int dist = node.weight;
            if (startX == exitX && startY == exitY) {
                minDist = dist;
                smallestNode = node;
                break;
            }
            for (int k = 0; k < 4; k++) {
                if (correctMatrix(matrix, visited, startX + row[k], startY + col[k])) {
                    visited[startX + row[k]][startY + col[k]] = true;
                    nodes.add(new Node(node, startX + row[k], startY + col[k], dist + 1));
                }
            }
        }
        return minDist;
    }

    private static class Node {

        Node prev;
        int posX;
        int posY;
        int weight;

        public Node(Node prev, int posX, int posY, int weight) {
            this.prev = prev;
            this.posX = posX;
            this.posY = posY;
            this.weight = weight;
        }
    }


}
