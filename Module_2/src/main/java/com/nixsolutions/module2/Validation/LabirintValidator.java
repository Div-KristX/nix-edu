/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.module2.Validation;

import com.nixsolutions.module2.Ecxeptions.IncorrectMeasurementsException;
import com.nixsolutions.module2.Ecxeptions.IncorrectPositionException;
import java.util.List;

/**
 * The {@code  LabirintValidator} class
 *
 * @author Krasnov Vladyslav
 */
public class LabirintValidator {

    public boolean labirintIsCorrect(List<String> labirint) {
        if (!linesIsCorrect(labirint)) {
            throw new RuntimeException(
                new IncorrectMeasurementsException("Incorrect lines length"));
        } else if (!columnsIsCorrect(labirint)) {
            throw new RuntimeException(
                new IncorrectMeasurementsException("Incorrect lines format"));
        } else {
            return true;
        }
    }

    public boolean startPointIsCorrect(List<String> labirint, int startY, int startX) {
        return checkPoint(labirint, startY, startX);
    }

    public boolean exitPointIsCorrect(List<String> labirint, int exitY, int exitX) {
        return checkPoint(labirint, exitY, exitX);
    }

    private boolean checkPoint(List<String> labirint, int pointY, int pointX) {
        if (pointX >= labirint.size() || pointX < 0) {
            throw new IllegalArgumentException("Incorrect position");
        } else if (pointY >= labirint.get(0).length() || pointY < 0) {
            throw new IllegalArgumentException("Incorrect position");
        } else {
            if (labirint.get(pointY).charAt(pointX) == '+') {
                return true;
            } else {
                throw new RuntimeException(
                    new IncorrectPositionException("Is not point to start or exit", pointY,
                        pointX));
            }
        }
    }

    private boolean linesIsCorrect(List<String> labirint) {
        final int LINE_LENGTH = labirint.get(0).length();
        return labirint.size() == labirint.stream()
            .filter(line -> line.length() == LINE_LENGTH)
            .count();
    }

    private boolean columnsIsCorrect(List<String> labirint) {
        return labirint.size() == labirint.stream()
            .filter(line -> {
                char[] chars = line.toCharArray();
                boolean plusOrDash = true;
                for (char c : chars) {
                    if (c != '+' && c != '-') {
                        if (c != '\n') {
                            plusOrDash = false;
                            break;
                        }
                    }
                }
                return plusOrDash;
            }).count();
    }

}
