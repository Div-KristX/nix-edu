package com.nixsolutions.module2.Ecxeptions;

public class IncorrectMeasurementsException extends Exception {

    public IncorrectMeasurementsException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "Incorrect dimensions of the labirint";
    }
}
