package com.nixsolutions.module2.Ecxeptions;

public class IncorrectPositionException extends Exception {

    private final int posX;
    private final int posY;
    private final String message;

    public IncorrectPositionException(String message, int posX, int posY) {
        super(message);
        this.posX = posX;
        this.posY = posY;
        this.message = null;
    }

    public IncorrectPositionException(String message) {
        super(message);
        posX = 0;
        posY = 0;
        this.message = message;
    }

    @Override
    public String toString() {
        if (message == null) {
            return "Incorrect point" + " " + posY + " " + posX;
        } else {
            return message;
        }

    }
}
