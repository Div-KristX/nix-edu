/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.module2;

import com.nixsolutions.module2.LabirintBuilder.LabirintBuilder;

/**
 * The {@code  Labirint} class
 *
 * @author Krasnov Vladyslav
 */
public class Labirint {

    public void escapeFromLabirint() {
        LabirintBuilder builder = new LabirintBuilder();
        LabirintEscape escape = builder.createEscape();
        escape.findWay();
    }
}
