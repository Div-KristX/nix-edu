/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.module2.LabirintBuilder;

import com.nixsolutions.module2.LabirintEscape;
import com.nixsolutions.module2.LabirintParser.LabirintParser;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * The {@code  LabirintBuilder} class
 *
 * @author Krasnov Vladyslav
 */
public class LabirintBuilder {

    private Path path;
    private int startY;
    private int startX;
    private int exitY;
    private int exitX;

    public LabirintBuilder() {
        initLabirint();
    }

    public void initLabirint() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Print path ");
        path = Path.of(scanner.nextLine());
        System.out.print("Print start pos(Y) ");
        startY = scanner.nextInt();
        System.out.print("Print start pos(X) ");
        startX = scanner.nextInt();
        System.out.print("Print exit pos(Y) ");
        exitY = scanner.nextInt();
        System.out.print("Print exit pos(X) ");
        exitX = scanner.nextInt();
        System.out.println();
        scanner.close();
    }

    public LabirintEscape createEscape() {
        return new LabirintEscape(LabirintParser.getLabirint(path), startY, startX, exitY, exitX);
    }

}
