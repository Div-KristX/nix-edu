package com.nixsolutions.module2.LabirintParser;

import com.nixsolutions.module2.Labirint;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EndToEndTest {

    private FileInputStream input = null;
    private PrintStream captureSystemOut = null;
    private Labirint labirint;
    private final String labAnswer =
        "--+----+--------\n"
            + "--++++++----+---\n"
            + "-------+--+++---\n"
            + "-------+--+-----\n"
            + "-------+--+-----\n"
            + "-------++++-----\n"
            + "-------+-----+++\n"
            + "-------+++++++--\n"
            + "#######+--+--+++\n"
            + "------#---+-----\n"
            + "------#---+++---\n"
            + "------#---+-----\n"
            + "------#####-----\n"
            + "----------#-----\n"
            + "----------#-----\n"
            + "----------+-----";

    @Before
    public void setUp() throws IOException {
        labirint = new Labirint();
        input = new FileInputStream("resources" + File.separator + "End-To-EndTest.txt");
        captureSystemOut = new PrintStream(
            new FileOutputStream("resources" + File.separator + "EscapeTest_ParserTest.txt"));
        System.setIn(input);
        System.setOut(captureSystemOut);
    }

    @After
    public void tearDown() {
        input = null;
    }

    @Test
    public void FullTest() {
        labirint.escapeFromLabirint();
        StringBuilder concat = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(
            new FileReader("resources" + File.separator + "EscapeTest_ParserTest.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                concat.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        concat.delete(0, 86);
        concat.delete(concat.length() - 1, concat.length());
        Assert.assertEquals(concat.toString(), labAnswer);
    }
}
