package com.nixsolutions.module2.LabirintParser;

import com.nixsolutions.module2.LabirintEscape;
import com.nixsolutions.module2.Validation.LabirintValidator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LabirintParserTest {

    private LabirintValidator validator;
    private Path path;
    private List<String> labirint;
    private LabirintEscape escape;
    private PrintStream captureSystemOut = null;
    private final String lab =
        "--+----+--------\n"
            + "--++++++----+---\n"
            + "-------+--+++---\n"
            + "-------+--+-----\n"
            + "-------+--+-----\n"
            + "-------++++-----\n"
            + "-------+-----+++\n"
            + "-------+++++++--\n"
            + "++++++++--+--+++\n"
            + "------+---+-----\n"
            + "------+---+++---\n"
            + "------+---+-----\n"
            + "------+++++-----\n"
            + "----------+-----\n"
            + "----------+-----\n"
            + "----------+-----";

    private final String labAnswer =
        "--+----+--------\n"
            + "--++++++----+---\n"
            + "-------+--+++---\n"
            + "-------+--+-----\n"
            + "-------+--+-----\n"
            + "-------++++-----\n"
            + "-------+-----+++\n"
            + "-------+++++++--\n"
            + "#######+--+--+++\n"
            + "------#---+-----\n"
            + "------#---+++---\n"
            + "------#---+-----\n"
            + "------#####-----\n"
            + "----------#-----\n"
            + "----------#-----\n"
            + "----------#-----";

    @Before
    public void setUp() throws FileNotFoundException {
        validator = new LabirintValidator();
        path = Path.of("resources" + File.separator + "LabirintTestCustom.txt");
        labirint = LabirintParser.getLabirint(path);
        escape = new LabirintEscape(labirint, 8, 0, 15, 10);
        captureSystemOut = new PrintStream(
            new FileOutputStream("resources" + File.separator + "EscapeTest_ParserTest.txt"));
        System.setOut(captureSystemOut);
    }

    @After
    public void tearDown() {
        validator = null;
        path = null;
        labirint = null;
    }

    @Test
    public void tryToParseLabirint() {
        StringBuilder concat = new StringBuilder();
        for (String text : LabirintParser.getLabirint(path)) {
            concat.append(text);
        }
        concat.delete(concat.length() - 1, concat.length());
        Assert.assertEquals(concat.toString(), lab);
    }

    @Test
    public void labirintValidation() {
        Assert.assertTrue(validator.labirintIsCorrect(labirint));
    }

    @Test
    public void startPointValidation() {
        Assert.assertTrue(validator.startPointIsCorrect(labirint, 8, 0));
    }

    @Test
    public void ExitPointValidation() {
        Assert.assertTrue(validator.exitPointIsCorrect(labirint, 15, 10));
    }

    @Test
    public void EscapeTest() throws FileNotFoundException {

        escape.findWay();
        StringBuilder concat = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(
            new FileReader("resources" + File.separator + "EscapeTest_ParserTest.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                concat.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        concat.delete(concat.length() - 1, concat.length());
        Assert.assertEquals(concat.toString(), labAnswer);
    }
}