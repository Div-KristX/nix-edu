/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

import java.nio.file.Path;
import java.util.Scanner;

/**
 * The {@code  PathParser} class
 *
 * @author Krasnov Vladyslav
 */
public class PathParser {

    /**
     * Returns a path from the input
     *
     * @return Path
     */
    public static Path getPath() {
        System.out.print("Write a path ");
        Scanner scanner = new Scanner(System.in);
        return Path.of(scanner.nextLine());
    }
}
