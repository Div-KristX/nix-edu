/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@code  ContentFinder} class
 *
 * @author Krasnov Vladyslav
 */
public class ContextFinder {


    /**
     * Gets a text form files or a file, by Regex pattern.<br> If the path is the file, the method
     * will print, the file`s absolute path and the text that Regex will find. <br> If path is the
     * directory, the method will print, the absolute path to every file and the text that Regex
     * will find.
     *
     * @param path  Path to a file or a directory
     * @param regex Regex pattern
     */
    public void getContextInFile(Path path, Pattern regex) {
        if (path == null || regex == null) {
            throw new IllegalArgumentException();
        }
        if (path.toFile().isFile()) {
            System.out.println(path.toAbsolutePath());
            System.out.println(getRegexGroups(getTextInFile(path.toFile()), regex));
        } else {
            TreeDirectoryStructure directoryVisitor = new TreeDirectoryStructure(path);
            for (File file : directoryVisitor.getFiles()) {
                System.out.println("\n" + file.toPath().toAbsolutePath());
                System.out.println(getRegexGroups(getTextInFile(file), regex));
            }
        }
    }

    /**
     * Returns a file`s text.
     *
     * @param path Path to the file.
     * @return String from this file.
     */
    private StringBuilder getTextInFile(File path) {
        StringBuilder wholeText = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = reader.readLine()) != null) {
                wholeText.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wholeText;
    }

    /**
     * Returns a founded text by a regex, and separates them as [text] [text2]
     *
     * @param text  Input text
     * @param regex Regex pattern
     * @return Founded regex groups
     */
    private String getRegexGroups(StringBuilder text, Pattern regex) {
        Matcher groupMatcher = regex.matcher(text);
        while (groupMatcher.find()) {
            text.replace(groupMatcher.start(), groupMatcher.end(),
                "[" + text.substring(groupMatcher.start(), groupMatcher.end()) + "]");
        }
        return text.toString();
    }
}
