/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * The {@code  RegexParser} class
 *
 * @author Krasnov Vladyslav
 */
public class RegexParser {

    /**
     * Returns a regex pattern form the input
     *
     * @return Pattern
     */
    public static Pattern getPattern() {
        System.out.print("Write a pattern ");
        Scanner scanner = new Scanner(System.in);
        return Pattern.compile(scanner.nextLine());
    }
}
