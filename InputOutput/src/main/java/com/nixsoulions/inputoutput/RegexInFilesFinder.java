/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

/**
 * The {@code  RegexInFilesFinder} class
 *
 * @author Krasnov Vladyslav
 */
public class RegexInFilesFinder {

    /**
     * User`s API to use a path and a regex pattern in an input.
     */
    public void getTextInFiles() {
        ContextFinder content = new ContextFinder();
        content.getContextInFile(PathParser.getPath(), RegexParser.getPattern());
    }
}
