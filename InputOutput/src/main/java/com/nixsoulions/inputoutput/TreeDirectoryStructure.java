/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code  TreeDirectoryStructure} class
 *
 * @author Krasnov Vladyslav
 */
public class TreeDirectoryStructure {

    private final List<File> allFiles;

    TreeDirectoryStructure(Path path) {
        allFiles = new ArrayList<>();
        searchFiles(path.toFile());
    }

    /**
     * Returns the list of a files
     *
     * @return List of a files
     */
    public List<File> getFiles() {
        return allFiles;
    }

    /**
     * Fills the list with files by founded files in directories.
     */
    private void searchFiles(File path) {
        if (path.isFile()) {
            allFiles.add(path);
        } else {
            File[] childFolder = path.listFiles();
            if (childFolder != null) {
                for (File child : childFolder) {
                    searchFiles(child);
                }
            }
        }
    }
}
