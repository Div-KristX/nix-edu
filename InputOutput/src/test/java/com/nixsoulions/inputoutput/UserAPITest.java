/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

/**
 * The {@code  UserAPITest} class
 *
 * @author Krasnov Vladyslav
 */
public class UserAPITest {

    public static void main(String[] args) {
        RegexInFilesFinder finder = new RegexInFilesFinder();
        finder.getTextInFiles();
    }
}
