/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsoulions.inputoutput;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The {@code  ContentFinderTest} class
 *
 * @author Krasnov Vladyslav
 */
public class ContextFinderTest {


    private ContextFinder content;
    private Path path;
    private Pattern pattern;
    private TreeDirectoryStructure treeVisitor;


    @Before
    public void setUp() {
        path = Paths.get("./");
        pattern = Pattern.compile("([A-Z])\\w+");
        content = new ContextFinder();
        treeVisitor = new TreeDirectoryStructure(path);
    }

    @After
    public void tearDown() {
        path = null;
        pattern = null;
        content = null;
    }

    @Test
    public void RootDirectoryInfo_StartsWithCapitalLetters() {
        content.getContextInFile(path, pattern);
    }

    @Test
    public void AllFilesInRootDirectory() {
        System.out.println(treeVisitor.getFiles());
    }
}