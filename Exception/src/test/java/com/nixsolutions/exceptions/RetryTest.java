/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.exceptions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The {@code  RetryTest} class
 *
 * @author Krasnov Vladyslav
 */
public class RetryTest {

    private Block block;
    private Retry tryBlock;
    private int[] array;

    @Before
    public void setUp() {
        array = new int[]{-1, 0, 10, 4, 5, 4, 9, 0, -4, 4, 5, 19, 29, 41, 4};
        block = new ValuesFinder(array, 4);
        tryBlock = new Retry(block, 5);
    }

    @After
    public void tearDown() {
        array = null;
        block = null;
        tryBlock = null;
    }

    @Test
    public void manualRunWith5Attempts() throws Exception {
        for (int i = 0; i <= 5; i++) {
            try {
                block.run();
            } catch (Exception e) {
                System.err.println(e + " Attempt " + (i + 1));
            }
        }
    }

    @Test
    public void runWithTheRetryClassWith5Attempts() throws Exception {
        try {
            tryBlock.tryBlockCode();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}