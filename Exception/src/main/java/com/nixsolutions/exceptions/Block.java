/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.exceptions;

/**
 * The {@code  Block} interface
 *
 * @author Krasnov Vladyslav
 */
public interface Block {

    /**
     * The method to run.
     */
    void run() throws Exception;
}
