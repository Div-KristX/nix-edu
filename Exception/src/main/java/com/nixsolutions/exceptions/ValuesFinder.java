/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.exceptions;

import java.util.Arrays;
import java.util.Objects;

/**
 * The {@code  ValuesFinder} class
 *
 * @author Krasnov Vladyslav
 */
public class ValuesFinder implements Block {

    private final int[] inputArray;
    private final int[] indexes;
    private final int requiredValue;
    private int rightEdge;
    private int leftEdge;
    private int loopsCells;

    ValuesFinder(int[] inputArray, int requiredValue) {
        this.inputArray = inputArray;
        this.requiredValue = requiredValue;
        rightEdge = 0;
        leftEdge = inputArray.length - 1;
        loopsCells = inputArray.length & 1; // Even or not the length of the input array.
        // Checking the middle of the input array
        if (loopsCells == 1) {
            loopsCells = (inputArray.length / 2) + 1;
        } else {
            loopsCells = inputArray.length / 2;
        }

        indexes = new int[inputArray.length];
        Arrays.fill(indexes, -1);
    }

    /**
     * The method returns int array, of the positions of the required value, as values -1 or 0...n.
     * <br>Required: 5</br>
     * <br>Inout array: {1, 4, 5, 7, 8, 5, 3, 2, 5, 0}</br>
     * <br>Output array: {-1, -1, 2, -1, -1, 5, -1, -1, 8, -1}</br>
     *
     * @throws Exception throws if the search in the input array was not full.
     */
    @Override
    public void run() throws Exception {
        findIndexOnEdge(false);
        findIndexOnEdge(true);
        if (rightEdge != loopsCells && leftEdge != loopsCells) {
            throw new Exception("Search is not total");
        }
        System.out.println(Arrays.toString(indexes));
    }

    /**
     * The method finds the required value on the right or the left and writes down an index of this
     * value in the input array to the index array.
     *
     * @param right value of the side, {@code true} - for right side, {@code false} - for the left.
     */
    private void findIndexOnEdge(boolean right) {
        if (right) {
            if (collisionDetection(true)) {
                return;
            } else {
                if (requiredValue == inputArray[rightEdge]) {
                    indexes[rightEdge] = rightEdge;
                }
                rightEdge++;
            }
        } else {
            if (collisionDetection(false)) {
                return;
            } else {
                if (requiredValue == inputArray[leftEdge]) {
                    indexes[leftEdge] = leftEdge;
                }
                leftEdge--;
            }
        }
    }

    /**
     * The method detects the end of the iteration on the edges and pretends more loops and
     * exceptions.
     */
    private boolean collisionDetection(boolean right) {
        if (right) {
            return rightEdge == loopsCells;
        } else {
            return leftEdge == loopsCells;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ValuesFinder that = (ValuesFinder) o;
        return requiredValue == that.requiredValue && rightEdge == that.rightEdge
            && leftEdge == that.leftEdge && loopsCells == that.loopsCells && Arrays.equals(
            inputArray, that.inputArray) && Arrays.equals(indexes, that.indexes);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(requiredValue, rightEdge, leftEdge, loopsCells);
        result = 31 * result + Arrays.hashCode(inputArray);
        result = 31 * result + Arrays.hashCode(indexes);
        return result;
    }
}

