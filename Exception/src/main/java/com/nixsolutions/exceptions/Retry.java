/*
 * Copyright (c) 2022,
 * For NIX Solution
 */
package com.nixsolutions.exceptions;

/**
 * The {@code Retry} class
 *
 * @author Krasnov Vladyslav
 */
public class Retry {

    private final Block block;
    private final int requiredAttempts;

    Retry(Block block, int countOfAttempts) {
        this.block = block;
        this.requiredAttempts = countOfAttempts;
    }

    /**
     * Running input code with required attempts.
     *
     * @throws Exception throws when conditions was not reached.
     */

    public void tryBlockCode() throws Exception {
        int countOfAttempts = 0;
        Exception notFinished = new Exception();
        boolean fail = false;
        if (requiredAttempts <= 0) {
            throw new IllegalArgumentException("Value mast be greater than zero");
        }
        while (countOfAttempts <= requiredAttempts) {
            try {
                block.run();
                fail = false;
            } catch (Exception e) {
                notFinished = e;
                e.printStackTrace();
                fail = true;
            }
            if (fail) {
                try {
                    long millis = 100L * (countOfAttempts + 1);
                    System.out.println("Next attempt " + (countOfAttempts + 1)  + " in " + millis + "ms");
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                countOfAttempts = requiredAttempts;
            }
            countOfAttempts++;
        }
        if (fail) {
            throw notFinished;
        }
    }
}
